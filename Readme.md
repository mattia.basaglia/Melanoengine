Melanoengine
============

A 2D Game engine with scripting capabilities.


License
-------

GPLv3 or later, see COPYING.


Sources
-------

Up to date sources are available at https://gitlab.com/mattia.basaglia/Melanoengine


Contacts
--------

Mattia Basaglia <mattia.basaglia@gmail.com>


Installing
==========

The git repository makes use of submodules so ensure they are up to date
before compiling.


Dependencies
------------

* [C++14 Compiler](http://en.cppreference.com/w/cpp/compiler_support)
* [CMake](http://www.cmake.org/)
* [CPython API](https://www.python.org/)
* [Boost](http://www.boost.org/)
    * filesystem
    * program_options
    * python
* [SDL 2](https://www.libsdl.org/)
    * SDL_image
    * SDL_ttf


Building
--------

    mkdir build && cd build && cmake .. && make


Running
-------

    bin/melanoengine

See with `--help` for command line options.


Development
===========


Testing
-------

To compile the test cases, you need Boost unit_test_framework.
If you configured cmake without it, you'll need to re-run cmake.

There are several test targets:
* tests_compile         -- Compiles all of the tests
* tests_run             -- Compiles an runs all the tests
* tests_coverage        -- Runs all the tests and builds a coverage report (Requires [lcov](http://ltp.sourceforge.net/coverage/lcov.php))
* tests_coverage_view   -- Opens the coverage report in a browser


Code Documentation
------------------

You can generate the [Doxygen](http://www.doxygen.org/) documentation using `make doc`.


Python Module
-------------

You can build a python module with

    make python_module

This will create a library in (build)/lib that can be imported from python (v3+).
The module has the same capabilities as the one provided for scripts running
from the engine itself, except that the engine will not be initialized.
