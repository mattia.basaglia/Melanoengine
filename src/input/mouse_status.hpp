/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_INPUT_MOUSE_STATUS_HPP
#define MELANOENGINE_INPUT_MOUSE_STATUS_HPP

#include "graphics/geometry.hpp"

namespace input{


class MouseStatus
{
public:
    using MouseButtons = unsigned;

    enum MouseButton : MouseButtons
    {
        NoButton        = 0x000,
        LeftButton      = 0x001,
        RightButton     = 0x002,
        MiddleButton    = 0x004,
        X1Button        = 0x008,
        X2Button        = 0x010,
    };

    graphics::Point pos;
    MouseButtons    buttons = NoButton;
};

} // namespace input
#endif // MELANOENGINE_INPUT_MOUSE_STATUS_HPP
