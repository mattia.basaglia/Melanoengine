/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_INPUT_KEYBOARD_STATUS_HPP
#define MELANOENGINE_INPUT_KEYBOARD_STATUS_HPP

#include <set>
#include "backend/common/backend.hpp"
#include "backend/common/input/input_backend.hpp"
#include "backend/common/input/key_data.hpp"
#include "scan_code.hpp"

namespace input{

class Key
{
public:
    explicit Key(KeyValue key)
        : data(backend::Backend::instance().input().create_key(key))
    {}

    explicit Key(ScanCode code)
        : data(backend::Backend::instance().input().create_key(code))
    {}

    explicit Key(const std::string& key_name)
        : data(backend::Backend::instance().input().create_key(key_name))
    {}

    /**
     * \brief Human-readable key name
     */
    std::string name() const
    {
        return data->name();
    }

    /**
     * \brief Physical code
     */
    ScanCode code() const
    {
        return data->code();
    }

    /**
     * \brief Layout value for the key
     */
    KeyValue value() const
    {
        return data->value();
    }

    /**
     * \brief Whether the key is being pressed
     */
    bool is_down() const
    {
        return data->is_down();
    }

private:
    std::unique_ptr<backend::KeyData> data;
};


class KeyboardStatus
{
public:
    explicit KeyboardStatus(std::set<ScanCode> codes)
        : codes(std::move(codes))
    {}

    bool is_down(const Key& key) const
    {
        return codes.count(key.code());
    }

    bool is_down(ScanCode code) const
    {
        return codes.count(code);
    }

private:
    std::set<ScanCode> codes;
};

} // namespace input
#endif // MELANOENGINE_INPUT_KEYBOARD_STATUS_HPP
