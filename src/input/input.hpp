/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_INPUT_INPUT_HPP
#define MELANOENGINE_INPUT_INPUT_HPP

#include "backend/common/backend.hpp"
#include "backend/common/input/input_backend.hpp"
#include "backend/common/input/input_data.hpp"

#include "input/keyboard_status.hpp"

namespace input{

class Input
{
public:
    Input() : data(backend::Backend::instance().input().create_input_status())
    {}

    /**
     * \brief Returns the mouse status
     * \param global If \b true, coordinates are relative to the desktop
     *               otherwise relative to the currently focused window
     */
    MouseStatus mouse_status(bool global = false)
    {
        return data->mouse_status(global);
    }

    KeyboardStatus keyboard_status()
    {
        return KeyboardStatus(data->keyboard_status());
    }

private:
    std::unique_ptr<backend::InputData> data;
};

} // namespace input
#endif // MELANOENGINE_INPUT_INPUT_HPP
