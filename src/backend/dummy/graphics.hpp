/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_DUMMY_GRAPHICS_HPP
#define MELANOENGINE_BACKEND_DUMMY_GRAPHICS_HPP

#include "backend/common/graphics/graphic_backend.hpp"

namespace backend{
namespace dummy{

class DummyWindow : public WindowData
{
public:
    DummyWindow(
        graphics::Rectangle rect,
        const std::string& title,
        bool fullscreen
    ) : _rect(rect), _title(title), _fullscreen(fullscreen)
    {}

    graphics::Size size() const
    {
        return rect().size();
    }

    graphics::Point pos() const
    {
        return rect().top_left();
    }

    graphics::Rectangle rect() const
    {
        return _rect;
    }

    void move(graphics::Point pos)
    {
        _rect.x = pos.x;
        _rect.y = pos.y;
    }

    void move_to_center()
    {
        _rect.x = 0;
        _rect.y = 0;
    }

    void resize(graphics::Size size)
    {
        _rect.width = size.width;
        _rect.height = size.height;
    }

    bool fullscreen() const
    {
        return _fullscreen;
    }

    void set_fullscreen(bool fullscreen)
    {
        _fullscreen = fullscreen;
    }

    std::string title() const
    {
        return _title;
    }

    void set_title(const std::string& title)
    {
        _title = title;
    }
private:
    graphics::Rectangle _rect;
    std::string _title;
    bool _fullscreen ;
};

class DummyPainter : public PainterData
{
public:
    void commit() override {}

    void fill(const graphics::Paint& style) override
    {}

    void draw_rect(const graphics::Paint& style,
                   const graphics::Rectangle& rect) override
    {}

    void draw_line(const graphics::Paint& style, const graphics::Line& line) override
    {}

    void draw_pixel(const graphics::Paint& style, const graphics::Point& pos) override
    {}

    void draw_image(
        ImageData* image,
        const graphics::Rectangle& source,
        const graphics::Rectangle& dest,
        bool horizontal_flip,
        bool vertical_flip
    ) override
    {}

    graphics::Rectangle draw_text(
        const graphics::FontPaint& style,
        FontData* font,
        const std::string& text,
        const graphics::Point& origin
    ) override
    {
        return {};
    }
};

class DummyImage : public ImageData
{
public:
    DummyImage(const std::string& name, graphics::Size size)
        : _name(name), _size(size)
    {}

    graphics::Size size() override
    {
        return _size;
    }

    std::string name() const
    {
        return _name;
    }

    void save(const std::string& filename) const override
    {
    }

private:
    std::string _name;
    graphics::Size _size;
};

class DummyFont : public FontData
{
public:
    DummyFont(
        const std::string& filename,
        std::size_t point_size,
        graphics::FontStyle style
    ) : _filename(filename), _point_size(point_size), _style(style)
    {}

    graphics::FontStyle style() override
    {
        return _style;
    }

    bool monospace()  override
    {
        return true;
    }

    graphics::Dimension point_size()  override
    {
        return _point_size;
    }


    graphics::Dimension max_height()  override
    {
        return _point_size;
    }

    graphics::Dimension ascent()  override
    {
        return _point_size / 2;
    }

    graphics::Dimension descent()
    {
        return _point_size / 2;
    }

    graphics::Dimension line_height() override
    {
        return _point_size;
    }

    std::string family() override
    {
        return _filename;
    }

    bool has_character(const melanolib::string::Unicode& unicode) override
    {
        return true;
    }

    graphics::GlyphMetrics glyph_metrics(const melanolib::string::Unicode& unicode) override
    {
        return {
            {
                graphics::Point(0, -_point_size/2),
                graphics::Size(_point_size, _point_size)
            },
            graphics::Point(_point_size, 0)
        };
    }

    graphics::Size size(const std::string& string) override
    {
        return graphics::Size(
            _point_size * string.size(),
            _point_size
        );
    }

private:
    std::string _filename;
    std::size_t _point_size;
    graphics::FontStyle _style;
};

/**
 * \brief Dummy back-end to test higher-level functionality
 */
class DummyGraphics : public GraphicBackend
{
public:
    static void register_dummy()
    {
        factory().add<DummyGraphics>("dummy", 0);
    }

    std::unique_ptr<WindowData> create_window(
        graphics::Rectangle rect,
        const std::string& title,
        bool fullscreen
    ) override
    {
        return std::make_unique<DummyWindow>(rect, title, fullscreen);
    }

    std::unique_ptr<WindowData> create_window(
        graphics::Size size,
        const std::string& title,
        bool fullscreen
    ) override
    {
        return create_window(graphics::Rectangle(graphics::Point(), size), title, fullscreen);
    }

    std::unique_ptr<PainterData> create_painter(WindowData* window) override
    {
        return std::make_unique<DummyPainter>();
    }

    std::unique_ptr<PainterData> create_painter(ImageData* image) override
    {
        return std::make_unique<DummyPainter>();
    }

    std::shared_ptr<ImageData> create_image(const std::string& filename) override
    {
        return std::make_shared<DummyImage>(filename, graphics::Size(1, 1));
    }

    std::shared_ptr<ImageData> create_image(
        const graphics::Size& size,
        const graphics::Color& color
    ) override
    {
        return std::make_shared<DummyImage>(color.format(), size);
    }


    std::shared_ptr<ImageData> create_image(WindowData* window) override
    {
        return std::make_shared<DummyImage>("Window " + window->title(), window->size());
    }

    std::shared_ptr<FontData> create_font(
        const std::string& filename,
        std::size_t point_size,
        graphics::FontStyle style
    ) override
    {
        return std::make_shared<DummyFont>(filename, point_size, style);
    }

};

} // namespace dummy
} // namespace backend
#endif // MELANOENGINE_BACKEND_DUMMY_GRAPHICS_HPP
