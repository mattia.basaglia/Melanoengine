/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_HPP
#define MELANOENGINE_BACKEND_HPP

#include <memory>
#include "melanolib/utils/singleton.hpp"

namespace backend {

class GraphicBackend;
class InputBackend;

class Backend : public melanolib::Singleton<Backend>
{
public:
    /**
     * \brief Creates all the concrete back-end objects
     * \throws engine::Error if already initialized
     */
    void initialize(
        const std::string& graphic,
        const std::string& input
    );

    /**
     * \throws engine::Error if not initialized
     */
    void require_initialized() const;

    /**
     * \brief Whether the back-end system has been initialized
     */
    bool initialized() const
    {
        return _initialized;
    }

    GraphicBackend& graphic() const
    {
        return *_graphic;
    }

    InputBackend& input() const
    {
        return *_input;
    }

    /**
     * \brief Loads all avaible backend classes in their factories
     */
    static void load_factories();

private:
    Backend();
    ~Backend();
    friend ParentSingleton;

    std::unique_ptr<GraphicBackend> _graphic;
    std::unique_ptr<InputBackend> _input;

    static bool _factories_loaded;
    bool _initialized = false;
};

} // namespace backend
#endif // MELANOENGINE_BACKEND_HPP
