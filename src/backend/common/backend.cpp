/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "backend/common/backend.hpp"
#include "backend/common/types.hpp"

#include <iostream>

namespace backend {

void Backend::initialize(
    const std::string& graphic,
    const std::string& input
)
{
    load_factories();

    if ( _initialized )
        throw engine::Error("Already initialized", "Backend");
    _initialized = true;

    _graphic = GraphicBackend::factory().create(graphic);
    _input = InputBackend::factory().create(input);
}

void Backend::require_initialized() const
{
    if ( !_initialized )
        throw engine::Error("Not initialized", "Backend");
}

Backend::Backend() = default;
Backend::~Backend() = default;

} // namespace backend
