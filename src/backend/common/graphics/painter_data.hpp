/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_COMMON_GRAPHICS_PAINTER_DATA_HPP
#define MELANOENGINE_BACKEND_COMMON_GRAPHICS_PAINTER_DATA_HPP

#include <boost/core/noncopyable.hpp>
#include "graphics/paint.hpp"

namespace backend{
class ImageData;
class FontData;

class PainterData : private boost::noncopyable
{
public:
    virtual ~PainterData(){}

    /**
     * \brief Ensures all paint operations appear on the target
     */
    virtual void commit() = 0;

    /**
     * \brief Fills the target with the given color
     */
    virtual void fill(const graphics::Paint& style) = 0;

    /**
     * \brief Fills a rectangle with a solid color
     */
    virtual void draw_rect(const graphics::Paint& style,
                           const graphics::Rectangle& rect) = 0;

    /**
     * \brief Draws a a line
     */
    virtual void draw_line(const graphics::Paint& style, const graphics::Line& line) = 0;

    /**
     * \brief Draws a pixel
     */
    virtual void draw_pixel(const graphics::Paint& style, const graphics::Point& pos) = 0;

    /**
     * \brief Draws a subset of the image stretching it to fill the destination
     * \pre \p image has been created by the same backend as the target
     */
    virtual void draw_image(
        ImageData* image,
        const graphics::Rectangle& source,
        const graphics::Rectangle& dest,
        bool horizontal_flip,
        bool vertical_flip
    ) = 0;

    /**
     * \brief Draws the given piece of text in the selected style
     * \param style     Text style
     * \param font      Font to use
     * \param text      Utf-8 encoded string
     * \param origin    Its semantics are defined by \p style.vertical_align
     * \returns The bounding box of the rendered text
     * \pre \p font has been created by the same back-end as the target
     */
    virtual graphics::Rectangle draw_text(
        const graphics::FontPaint& style,
        FontData* font,
        const std::string& text,
        const graphics::Point& origin
    ) = 0;
};

} // namespace backend
#endif // MELANOENGINE_BACKEND_COMMON_GRAPHICS_PAINTER_DATA_HPP
