/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_WINDOW_DATA_HPP
#define MELANOENGINE_BACKEND_WINDOW_DATA_HPP

#include <string>
#include <boost/core/noncopyable.hpp>
#include "graphics/geometry.hpp"

namespace backend {

/**
 * \brief A windows on the desktop environment
 */
class WindowData : private boost::noncopyable
{
public:
    virtual ~WindowData(){}

    virtual graphics::Size size() const = 0;
    virtual graphics::Point pos() const = 0;
    virtual graphics::Rectangle rect() const = 0;

    virtual void move(graphics::Point pos) = 0;
    virtual void move_to_center() = 0;
    virtual void resize(graphics::Size size) = 0;

    virtual bool fullscreen() const = 0;
    virtual void set_fullscreen(bool fullscreen) = 0;

    virtual std::string title() const = 0;
    virtual void set_title(const std::string& title) = 0;
};

} // namespace backend
#endif // MELANOENGINE_BACKEND_WINDOW_HPP
