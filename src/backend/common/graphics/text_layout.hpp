/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_COMMON_GRAPHICS_TEXT_LAYOUT_HPP
#define MELANOENGINE_BACKEND_COMMON_GRAPHICS_TEXT_LAYOUT_HPP

#include "melanolib/string/string_view.hpp"

#include "font_data.hpp"
#include "graphics/font_paint.hpp"

namespace backend{

/**
 * \brief Class that helps rendering multi-line text for back-ends that don't
 *        offer this feature built-in
 */
class TextLayout
{
public:
    struct value_type
    {
        melanolib::cstring_view text;
        graphics::Rectangle box;
    };

    using iterator = std::vector<value_type>::const_iterator;


    explicit TextLayout(
        FontData* font,
        melanolib::cstring_view text,
        const graphics::FontPaint& style
    ) : font(font),
        line_height(melanolib::math::round(font->line_height() * style.line_separation)),
        tab_width(font->point_size()),
        v_align(style.vertical_align)
    {
        append(text);
    }

    /**
     * \brief Splits \p text and appends the resulting chunks
     */
    void append(melanolib::cstring_view text);

    bool empty() const
    {
        return chunks.empty();
    }

    iterator begin() const
    {
        return chunks.begin();
    }

    iterator end() const
    {
        return chunks.end();
    }

    /**
     * \brief Evaluates the minimum rectangle that can contain all of the chunks
     */
    graphics::Rectangle bounding_box() const;

private:
    /**
     * \brief Pushes the current chunk in chunks, skipping the character at \p end
     */
    void push_chunk(melanolib::cstring_view::iterator end);

    /**
     * \brief Creates a chunk from the given string range
     */
    value_type make_chunk(melanolib::cstring_view::iterator begin,
                          melanolib::cstring_view::iterator end);

    FontData* font;
    std::vector<value_type> chunks;
    melanolib::cstring_view::iterator chunk_begin;
    graphics::Point chunk_pos;
    graphics::Dimension line_height;
    graphics::Dimension tab_width;
    graphics::TextVerticalAlign v_align;
};

} // namespace backend


#endif // MELANOENGINE_BACKEND_COMMON_GRAPHICS_TEXT_LAYOUT_HPP
