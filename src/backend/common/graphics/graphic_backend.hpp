/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GRAPHIC_BACKEND_HPP
#define MELANOENGINE_GRAPHIC_BACKEND_HPP

#include "backend/common/backend_base.hpp"
#include "backend/common/graphics/window_data.hpp"
#include "backend/common/graphics/painter_data.hpp"
#include "backend/common/graphics/image_data.hpp"
#include "backend/common/graphics/font_data.hpp"

namespace backend {

/**
 * \brief Back-end class.
 *
 * This is a virtual base for back-end implementations.
 * A back-end is responsible for graphical output.
 */
class GraphicBackend : public BackendBase<GraphicBackend>
{
public:
    /**
     * \brief Creates a window
     */
    virtual std::unique_ptr<WindowData> create_window(
        graphics::Rectangle rect,
        const std::string& title,
        bool fullscreen
    ) = 0;

    /**
     * \brief Creates a centered window
     */
    virtual std::unique_ptr<WindowData> create_window(
        graphics::Size size,
        const std::string& title,
        bool fullscreen
    ) = 0;

    /**
     * \brief Creates a painter that draws on the given window
     * \pre \p window has been created by the same backend
     */
    virtual std::unique_ptr<PainterData> create_painter(WindowData* window) = 0;

    /**
     * \brief Creates a painter that draws on the given image
     * \pre \p image has been created by the same backend
     */
    virtual std::unique_ptr<PainterData> create_painter(ImageData* image) = 0;

    /**
     * \brief Creates an image from a file
     */
    virtual std::shared_ptr<ImageData> create_image(const std::string& filename) = 0;

    /**
     * \brief Creates a new image
     */
    virtual std::shared_ptr<ImageData> create_image(
        const graphics::Size& size,
        const graphics::Color& color
    ) = 0;

    /**
     * \brief Creates a screenshot from the window
     * \pre \p window has been created by the same backend
     */
    virtual std::shared_ptr<ImageData> create_image(WindowData* window) = 0;

    /**
     * \brief Loads the given font with the given style and size
     */
    virtual std::shared_ptr<FontData> create_font(
        const std::string& filename,
        std::size_t point_size,
        graphics::FontStyle style
    ) = 0;

};

} // namespace backend
#endif // MELANOENGINE_GRAPHIC_BACKEND_HPP
