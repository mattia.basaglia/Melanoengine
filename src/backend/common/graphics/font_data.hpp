/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_COMMON_GRAPHICS_FONT_DATA_HPP
#define MELANOENGINE_BACKEND_COMMON_GRAPHICS_FONT_DATA_HPP

#include "melanolib/string/encoding.hpp"

#include "graphics/font_style.hpp"
#include "graphics/geometry.hpp"

namespace backend{

class FontData
{
public:
    virtual ~FontData(){}

    virtual graphics::FontStyle style() = 0;

    /**
     * \brief Whether all glyphs have the same width
     */
    virtual bool monospace() = 0;

    /**
     * \brief Point size of the font
     */
    virtual graphics::Dimension point_size() = 0;

    /**
     * \brief Maximum height of a glyph
     */
    virtual graphics::Dimension max_height() = 0;

    /**
     * \brief Distance of the top of an ascender from the baseline
     */
    virtual graphics::Dimension ascent() = 0;
    /**
     * \brief Distance of the top of an descender from the baseline
     */
    virtual graphics::Dimension descent() = 0;

    /**
     * \brief Recommended distance between two baselines
     */
    virtual graphics::Dimension line_height() = 0;

    /**
     * \brief Font family name
     */
    virtual std::string family() = 0;

    /**
     * \brief Whether the font can render the given character
     */
    virtual bool has_character(const melanolib::string::Unicode& unicode) = 0;

    /**
     * \brief Describes graphical properties of a rendered glyph
     */
    virtual graphics::GlyphMetrics glyph_metrics(const melanolib::string::Unicode& unicode) = 0;

    /**
     * \brief Size the given string will take when rendered
     * \note \p string must be encoded in utf-8
     */
    virtual graphics::Size size(const std::string& string) = 0;
};


} // namespace backend
#endif // MELANOENGINE_BACKEND_COMMON_GRAPHICS_FONT_DATA_HPP
