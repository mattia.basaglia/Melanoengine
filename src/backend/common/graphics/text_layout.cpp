/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "text_layout.hpp"

namespace backend{

graphics::Rectangle TextLayout::bounding_box() const
{
    graphics::Rectangle bounding_box(graphics::Point(0, 0), graphics::Size(0, 0));
    for ( const auto& chunk : chunks )
    {
        bounding_box |= chunk.box;
    }

    switch ( v_align )
    {
        case graphics::TextVerticalAlign::Baseline:
            bounding_box.y -= font->ascent();
            break;
        case graphics::TextVerticalAlign::Top:
            break;
        case graphics::TextVerticalAlign::Bottom:
            bounding_box.y -= bounding_box.height;
            break;
        case graphics::TextVerticalAlign::Middle:
            bounding_box.y -= bounding_box.height / 2;
            break;
    }

    return bounding_box;
}

void TextLayout::append(melanolib::cstring_view text)
{
    chunk_begin = text.begin();
    for ( auto it = text.begin(); it != text.end(); ++it )
    {
        switch ( *it )
        {
            case '\n':
                push_chunk(it);
                chunk_pos.x = 0;
                chunk_pos.y += line_height;
                break;
            case '\b':
                push_chunk(it);
                if ( !chunks.empty() )
                {
                    auto& last_chunk = chunks.back();
                    if ( last_chunk.text.size() == 1 )
                    {
                        chunks.pop_back();
                        if ( chunks.empty() )
                            chunk_pos = {0, 0};
                        else
                            chunk_pos.x = chunks.back().box.right();
                    }
                    else
                    {
                        auto old_x = chunks.back().box.x;
                        last_chunk = make_chunk(
                            last_chunk.text.begin(),
                            last_chunk.text.end() - 1
                        );
                        chunks.back().box.x = old_x;
                        chunk_pos.x = chunks.back().box.right();
                    }
                }
                break;
            case '\r':
                push_chunk(it);
                while ( !chunks.empty() && chunks.back().box.x >= 0 && chunks.back().box.y >= chunk_pos.y )
                    chunks.pop_back();
                chunk_pos.x = 0;
                break;
            case '\t':
                push_chunk(it);
                chunk_pos.x += tab_width;
                break;
            case '\v':
                push_chunk(it);
                chunk_pos.y += line_height;
                break;
        }
    }
    push_chunk(text.end());
}

void TextLayout::push_chunk(melanolib::cstring_view::iterator end)
{
    if ( end > chunk_begin )
    {
        chunks.push_back(make_chunk(chunk_begin, end));
        chunk_pos.x += chunks.back().box.width;
    }

    chunk_begin = end + 1;
}

TextLayout::value_type TextLayout::make_chunk(
    melanolib::cstring_view::iterator begin,
    melanolib::cstring_view::iterator end)
{
    return {
        melanolib::cstring_view(begin, end),
        graphics::Rectangle(
            chunk_pos,
            font->size(std::string(begin, end))
        )
    };
}

} // namespace backend
