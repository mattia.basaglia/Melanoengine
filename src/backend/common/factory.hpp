/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_FACTORY_HPP
#define MELANOENGINE_BACKEND_FACTORY_HPP

#include <map>
#include <memory>
#include <functional>
#include <vector>

#include "melanolib/utils/singleton.hpp"

#include "engine/error.hpp"

namespace backend {

/**
 * \brief Back-end factory
 */
template<class BackendBase>
class Factory : public melanolib::Singleton<Factory<BackendBase>>
{
public:
    using Wrapper = std::unique_ptr<BackendBase>;
    using Builder = std::function<Wrapper()>;

    /**
     * \brief Registers a functor with the given name
     * \param name      Name of the backend to register
     * \param function  Function object that returns a Wrapper
     * \param weight    Weight used to decide the default value (0 = never)
     * \throws Error
     */
    void add(const std::string& name, const Builder& function, float weight = 0)
    {
        if ( contains(name) )
            throw engine::Error("Backend already registered: " + name, "Backend");

        backends[name] = function;

        if ( weight > default_backend.second )
            default_backend.first = name;
    }

    /**
     * \brief Registers a class constructor with the given name
     * \param name      Name of the backend to register
     * \param weight    Weight used to decide the default value (0 = never)
     */
    template<class BackendT>
        void add(const std::string& name, float weight = 0)
    {
        add(name, []{ return std::make_unique<BackendT>(); }, weight);
    }

    /**
     * \brief Whether the factory contains a construction function with the
     * given name
     */
    bool contains(const std::string& name) const
    {
        return backends.count(name);
    }

    /**
     * \brief Creates an object from its constructor name
     * \throws Error
     */
    Wrapper create(const std::string& name) const
    {
        auto iter = backends.find(name);
        if ( iter == backends.end() )
            throw engine::Error("Backend not found: " + name, "Backend");
        return iter->second();
    }

    /**
     * \brief Returns a list of registered constructors
     */
    std::vector<std::string> available() const
    {
        std::vector<std::string> names;
        names.reserve(backends.size());
        for ( const auto& p : backends )
            names.push_back(p.first);
        return names;
    }

    /**
     * \brief Name of the default backend
     */
    std::string default_name() const
    {
        return default_backend.first;
    }

private:
    Factory(){}
    friend typename Factory::ParentSingleton;
    std::map<std::string, Builder> backends;
    std::pair<std::string, float> default_backend = {"", 0};
};

} // namespace backend
#endif // MELANOENGINE_BACKEND_FACTORY_HPP
