/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_BASE_HPP
#define MELANOENGINE_BACKEND_BASE_HPP

#include "backend/common/factory.hpp"

namespace backend {

/**
 * \brief Dummy base class for back-end types that avoids repeating some boilerplate
 * \note This is for base backends only, concrete back-ends shall derive from
 *       a more specific class
 */
template<class Derived>
class BackendBase
{
public:
    using Factory = backend::Factory<Derived>;
    static Factory& factory()
    {
        return Factory::instance();
    }

    virtual ~BackendBase(){}
};

} // namespace backend
#endif // MELANOENGINE_BACKEND_BASE_HPP
