/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_COMMON_INPUT_INPUT_BACKEND_HPP
#define MELANOENGINE_BACKEND_COMMON_INPUT_INPUT_BACKEND_HPP

#include "backend/common/backend_base.hpp"
#include "input/scan_code.hpp"


namespace backend {
class InputData;
class KeyData;

/**
 * \brief Back-end class.
 *
 * This is a virtual base for back-end implementations.
 * A back-end is responsible for input and events.
 */
class InputBackend : public BackendBase<InputBackend>
{
public:
    /**
     * \brief Creates a window
     */
    virtual std::unique_ptr<InputData> create_input_status() = 0;

    /**
     * \brief Ensures input events are being listened
     */
    virtual void listen() = 0;

    /**
     * \brief Creates a key from its scan code
     */
    virtual std::unique_ptr<KeyData> create_key(input::ScanCode code) = 0;

    /**
     * \brief Creates a key from its name
     */
    virtual std::unique_ptr<KeyData> create_key(const std::string& key_name) = 0;

    /**
     * \brief Creates a key from its layout value
     */
    virtual std::unique_ptr<KeyData> create_key(input::KeyValue key) = 0;
};
} // namespace backend
#endif // MELANOENGINE_BACKEND_COMMON_INPUT_INPUT_BACKEND_HPP
