/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_COMMON_INPUT_KEY_DATA_HPP
#define MELANOENGINE_BACKEND_COMMON_INPUT_KEY_DATA_HPP

#include <string>
#include "input/scan_code.hpp"

namespace backend{

class KeyData
{
public:
    virtual ~KeyData(){}

    /**
     * \brief Human-readable key name
     */
    virtual std::string name() const = 0;

    /**
     * \brief Physical code
     */
    virtual input::ScanCode code() const = 0;

    /**
     * \brief Layout value for the key
     */
    virtual input::KeyValue value() const = 0;

    /**
     * \brief Whether the key is being pressed
     */
    virtual bool is_down() const = 0;
};

} // namespace backend
#endif // MELANOENGINE_BACKEND_COMMON_INPUT_KEY_DATA_HPP
