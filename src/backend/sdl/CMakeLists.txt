# Copyright 2016-2017 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
set(BACKEND_NAME sdl)
set(BACKEND_LIBRARY backend_${BACKEND_NAME})

component_add(${BACKEND_LIBRARY} sdl_backend.cpp)

find_package(SDL2 REQUIRED image ttf)
find_package(PNG REQUIRED)
include_directories(${SDL2_INCLUDE_DIRS})
component_link(${BACKEND_LIBRARY}
    ${SDL2_LIBRARIES}
    ${PNG_LIBRARIES}
)

find_package(JPEG QUIET)
if(JPEG_FOUND)
    component_link(${BACKEND_LIBRARY} ${JPEG_LIBRARIES})
endif()

set(BACKENDS ${BACKENDS} sdl CACHE INTERNAL "")
