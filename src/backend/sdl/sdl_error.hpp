/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_SDL_ERROR_HPP
#define MELANOENGINE_BACKEND_SDL_ERROR_HPP

#include <SDL2/SDL.h>

#include "engine/error.hpp"

namespace backend {
namespace sdl {

/**
 * \brief Backend error that gets the error message from SDL
 */
class SdlError : public engine::Error
{
public:
    SdlError(): Error(SDL_GetError(), "SDL") {}
    SdlError(const std::string& msg) : Error(msg, "SDL") {}

    /**
     * \brief Wraps the return value of SDL calls that return an integer
     * (with the semantic of zero for success and non-zero for failure)
     * and throws an exception when appropriate
     */
    static void wrap_call(int error)
    {
        if ( error )
            throw SdlError();
    }
};

} // namespace sdl
} // namespace backend
#endif // MELANOENGINE_BACKEND_SDL_ERROR_HPP
