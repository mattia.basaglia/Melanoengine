/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_SDL_WINDOW_HPP
#define MELANOENGINE_BACKEND_SDL_WINDOW_HPP

#include "backend/common/graphics/window_data.hpp"
#include "backend/sdl/sdl_error.hpp"

namespace backend {
namespace sdl {

class SdlWindow : public WindowData
{
protected:
    explicit SdlWindow(SDL_Window* window)
    {
        if ( !window )
            throw SdlError();

        _window = window;

        _renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

        if ( !_renderer )
        {
            SDL_DestroyWindow(window);
            _window = nullptr;
            throw SdlError();
        }
    }

public:
    SdlWindow(
        graphics::Rectangle rect,
        const std::string& title = "",
        bool fullscreen = false)
    : SdlWindow(SDL_CreateWindow(
        title.c_str(),
        rect.x, rect.y,
        rect.width, rect.height,
        SDL_WINDOW_OPENGL | (fullscreen ? SDL_WINDOW_FULLSCREEN : 0)
    ))
    {}

    SdlWindow(
        graphics::Size size,
        const std::string& title = "",
        bool fullscreen = false)
    : SdlWindow(SDL_CreateWindow(
        title.c_str(),
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        size.width, size.height,
        SDL_WINDOW_OPENGL | (fullscreen ? SDL_WINDOW_FULLSCREEN : 0)
    ))
    {}

    ~SdlWindow()
    {
        SDL_DestroyRenderer(_renderer);
        SDL_DestroyWindow(_window);
    }

    graphics::Size size() const override
    {
        graphics::Size sz;
        SDL_GetWindowSize(_window, &sz.width, &sz.height);
        return sz;
    }

    graphics::Point pos() const override
    {
        graphics::Point pt;
        SDL_GetWindowPosition(_window, &pt.x, &pt.y);
        return pt;
    }

    graphics::Rectangle rect() const override
    {
        return {pos(), size()};
    }

    void move(graphics::Point pos) override
    {
        SDL_SetWindowPosition(_window, pos.x, pos.y);
    }

    void move_to_center() override
    {
        SDL_SetWindowPosition(_window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);
    }

    void resize(graphics::Size size) override
    {
        SDL_SetWindowSize(_window, size.width, size.height);
    }

    bool fullscreen() const override
    {
        return SDL_GetWindowFlags(_window) & (SDL_WINDOW_FULLSCREEN | SDL_WINDOW_FULLSCREEN_DESKTOP);
    }

    void set_fullscreen(bool fullscreen) override
    {
        SDL_SetWindowFullscreen(_window, fullscreen ? SDL_WINDOW_FULLSCREEN : 0);
    }

    std::string title() const override
    {
        return SDL_GetWindowTitle(_window);
    }

    void set_title(const std::string& title) override
    {
        SDL_SetWindowTitle(_window, title.c_str());
    }

    SDL_Renderer* renderer()
    {
        return _renderer;
    }

    SDL_Window* window()
    {
        return _window;
    }

private:
    SDL_Renderer* _renderer;
    SDL_Window* _window;
};

} // namespace sdl
} // namespace backend
#endif // MELANOENGINE_BACKEND_SDL_WINDOW_HPP
