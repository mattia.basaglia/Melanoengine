/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_SDL_SDL_PAINTER_HPP
#define MELANOENGINE_BACKEND_SDL_SDL_PAINTER_HPP

#include <map>

#include "backend/common/graphics/painter_data.hpp"
#include "backend/sdl/sdl_error.hpp"
#include "backend/sdl/sdl_image.hpp"
#include "backend/sdl/sdl_font.hpp"
#include "graphics/font_paint.hpp"
#include "backend/common/graphics/text_layout.hpp"

namespace backend{
namespace sdl{

class SdlPainter : public PainterData
{
public:
    explicit SdlPainter(SDL_Renderer* renderer, bool owning)
        : renderer(renderer), owning(owning)
    {
    }

    ~SdlPainter()
    {
        commit();

        for ( const auto& pair : textures )
            SDL_DestroyTexture(pair.second);

        if ( owning )
            SDL_DestroyRenderer(renderer);
    }

    void commit() override
    {
        SDL_RenderPresent(renderer);
    }

    void fill(const graphics::Paint& style) override
    {
        apply_style(style);
        if ( SDL_RenderClear(renderer) )
            throw SdlError();
    }

    void draw_rect(const graphics::Paint& style, const graphics::Rectangle& rect) override
    {
        apply_style(style);
        auto sdl_rect = make_rect(rect);
        if ( SDL_RenderFillRect(renderer, &sdl_rect) )
            throw SdlError();
    }

    void draw_line(const graphics::Paint& style, const graphics::Line& line) override
    {
        apply_style(style);
        if ( SDL_RenderDrawLine(renderer, line.p1.x, line.p1.y, line.p2.x, line.p2.y) )
            throw SdlError();
    }

    void draw_pixel(const graphics::Paint& style, const graphics::Point& pos) override
    {
        apply_style(style);
        if ( SDL_RenderDrawPoint(renderer, pos.x, pos.y) )
            throw SdlError();
    }

    void draw_image(
        ImageData* image,
        const graphics::Rectangle& source,
        const graphics::Rectangle& dest,
        bool horizontal_flip,
        bool vertical_flip
    ) override
    {

        auto src_rect = make_rect(source);
        auto dest_rect = make_rect(dest);

        SDL_RendererFlip flip = SDL_FLIP_NONE;
        if ( horizontal_flip )
            flip = SDL_RendererFlip(flip | SDL_FLIP_HORIZONTAL);
        if ( vertical_flip )
            flip = SDL_RendererFlip(flip | SDL_FLIP_VERTICAL);

        SdlError::wrap_call(SDL_RenderCopyEx(
            renderer,
            make_texture(static_cast<SdlImage*>(image), true),
            &src_rect, &dest_rect,
            0, nullptr, flip
        ));
    }

    graphics::Rectangle draw_text(
        const graphics::FontPaint& style,
        backend::FontData* font,
        const std::string& text,
        const graphics::Point& origin
    ) override
    {
        if ( text.empty() )
            return graphics::Rectangle(origin, graphics::Size{});

        SdlFont* sdl_font = static_cast<SdlFont*>(font);
        apply_blend_mode(style.blend_mode);

        TextLayout layout(sdl_font, text, style);

        SDL_Color color = make_color(style.color);
        auto box = layout.bounding_box().translated(origin);
        graphics::Point offset = box.top_left();

        for ( const auto& chunk : layout )
        {
            SdlImage image(sdl_font->render_line(color, chunk.text.str()));

            auto sdl_line_rect = make_rect(chunk.box.translated(offset));
            SdlError::wrap_call(SDL_RenderCopy(
                renderer,
                make_texture(&image, false),
                nullptr, &sdl_line_rect
            ));
        }

        return box;
    }

private:
    void apply_blend_mode(graphics::BlendMode mode)
    {
        SdlError::wrap_call(
            SDL_SetRenderDrawBlendMode(renderer, blend_mode(mode))
        );
    }

    void apply_style(const graphics::Paint& style)
    {
        apply_blend_mode(style.blend_mode);

        if ( style.color.valid() )
        {
            if ( SDL_SetRenderDrawColor(renderer, style.color.red(),
                    style.color.green(), style.color.blue(), style.color.alpha()) )
                throw SdlError();
        }
    }

    static SDL_BlendMode blend_mode(graphics::BlendMode blend_mode)
    {
        switch ( blend_mode )
        {
            case graphics::BlendMode::Alpha:return SDL_BLENDMODE_BLEND;
            case graphics::BlendMode::Add:  return SDL_BLENDMODE_ADD;
            default:                        return SDL_BLENDMODE_NONE;
        }
    }

    static SDL_Rect make_rect(const graphics::Rectangle& rect)
    {
        return {rect.x, rect.y, rect.width, rect.height};
    }

    static SDL_Color make_color(const graphics::Color& color)
    {
        return SDL_Color{ color.red(), color.green(), color.blue(), color.alpha() };
    }

    SDL_Texture* make_texture(SdlImage* sdl_image, bool cache)
    {
        if ( cache )
        {
            auto iter =  textures.find(sdl_image->surface());
            if ( iter != textures.end() )
                return iter->second;
        }

        SDL_Texture* texture = SDL_CreateTextureFromSurface(
            renderer, sdl_image->surface()
        );
        if ( !texture )
            throw SdlError();

        if ( cache )
            textures.insert({sdl_image->surface(), texture});

        return texture;
    }

    SDL_Renderer* renderer;
    std::map<SDL_Surface*, SDL_Texture*> textures;
    bool owning;
};

} // namespace sdl
} // namespace backend
#endif // MELANOENGINE_BACKEND_SDL_SDL_PAINTER_HPP
