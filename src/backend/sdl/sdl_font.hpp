/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_SDL_SDL_FONT_HPP
#define MELANOENGINE_BACKEND_SDL_SDL_FONT_HPP

#include <SDL2/SDL_ttf.h>

#include "backend/common/graphics/font_data.hpp"
#include "sdl_image.hpp"
#include "graphics/paint.hpp"

namespace backend{
namespace sdl{


class SdlFont : public FontData
{
public:
    SdlFont(const std::string& filename, graphics::Dimension point_size, graphics::FontStyle style)
        : _point_size(point_size)
    {
        _font = TTF_OpenFont(filename.c_str(), point_size);
        if ( !_font )
            throw SdlError();

        /// \note FontStyle values happen to match TTF_STYLE_* macros
        /// This means that is technically possible to create strikethrough
        /// fonts even if that value is not in FontStyle
        TTF_SetFontStyle(_font, style);

        TTF_SetFontHinting(_font, TTF_HINTING_LIGHT);
    }

    ~SdlFont()
    {
        TTF_CloseFont(_font);
    }

    graphics::FontStyle style() override
    {
        return TTF_GetFontStyle(_font);
    }

    bool monospace() override
    {
        return TTF_FontFaceIsFixedWidth(_font);
    }

    graphics::Dimension max_height() override
    {
        return TTF_FontHeight(_font);
    }

    graphics::Dimension ascent() override
    {
        return TTF_FontAscent(_font);
    }

    graphics::Dimension descent() override
    {
        return TTF_FontDescent(_font);
    }

    graphics::Dimension line_height() override
    {
        return TTF_FontLineSkip(_font);
    }

    std::string family() override
    {
        if ( char* name = TTF_FontFaceFamilyName(_font) )
            return name;
        return {};
    }

    bool has_character(const melanolib::string::Unicode& unicode) override
    {
        return TTF_GlyphIsProvided(_font, unicode.point());
    }

    graphics::GlyphMetrics glyph_metrics(const melanolib::string::Unicode& unicode) override
    {
        graphics::Point min;
        graphics::Point max;
        graphics::Dimension advance;
        SdlError::wrap_call(TTF_GlyphMetrics(
            _font, unicode.point(),
            &min.x, &min.y, &max.x, &max.y,
            &advance
        ));

        return graphics::GlyphMetrics({{min, max}, {advance, 0}});
    }

    graphics::Size size(const std::string& string) override
    {
        graphics::Size size;
        SdlError::wrap_call(TTF_SizeUTF8(_font, string.c_str(), &size.width, &size.height));
        return size;
    }

    /**
     * \brief Renders a single line of text as an image of the gien color
     */
    SDL_Surface* render_line(const SDL_Color& color, const std::string& string)
    {
        return TTF_RenderUTF8_Solid(_font, string.c_str(), color);
    }

    graphics::Dimension point_size() override
    {
        return _point_size;
    }

private:
    TTF_Font* _font;
    graphics::Dimension _point_size;
};



} // namespace sdl
} // namespace backend
#endif // MELANOENGINE_BACKEND_SDL_SDL_FONT_HPP
