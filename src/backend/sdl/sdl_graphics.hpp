/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_SDL_GRAPHICS_HPP
#define MELANOENGINE_BACKEND_SDL_GRAPHICS_HPP

#include <SDL2/SDL_image.h>

#include "sdl_backend.hpp"
#include "sdl_window.hpp"
#include "sdl_painter.hpp"
#include "sdl_image.hpp"
#include "sdl_font.hpp"
#include "backend/common/graphics/graphic_backend.hpp"

namespace backend {
namespace sdl {

class SdlGraphicsBackend : public GraphicBackend
{
public:
    SdlGraphicsBackend()
    {
        auto flags = IMG_INIT_PNG|IMG_INIT_JPG;
        if ( (IMG_Init(flags) & flags) != flags )
            throw SdlError();

        SdlError::wrap_call(TTF_Init());
    }

    ~SdlGraphicsBackend()
    {
        TTF_Quit();
        IMG_Quit();
    }

    std::unique_ptr<WindowData> create_window(
        melanolib::geo::geo_int::Size size,
        const std::string& title,
        bool fullscreen
    ) override
    {
        return std::make_unique<SdlWindow>(size, title, fullscreen);
    }

    std::unique_ptr<WindowData> create_window(
        melanolib::geo::geo_int::Rectangle rect,
        const std::string& title,
        bool fullscreen
    ) override
    {
        return std::make_unique<SdlWindow>(rect, title, fullscreen);
    }

    std::unique_ptr<PainterData> create_painter(WindowData* window) override
    {
        return std::make_unique<SdlPainter>(
            static_cast<SdlWindow*>(window)->renderer(),
            false
        );
    }

    std::unique_ptr<PainterData> create_painter(ImageData* image) override
    {
        auto renderer = SDL_CreateSoftwareRenderer(
            static_cast<SdlImage*>(image)->surface()
        );
        if ( !renderer )
            throw SdlError();
        return std::make_unique<SdlPainter>(renderer, true);
    }

    std::shared_ptr<ImageData> create_image(const graphics::Size& size,
                                            const graphics::Color& color) override
    {
        auto surface = SDL_CreateRGBSurface(
            0, size.width, size.height, 32, rmask, gmask, bmask, amask
        );
        if ( !surface )
            throw SdlError();
        /// \todo fill with \p color
        return std::make_shared<SdlImage>(surface);
    }

    std::shared_ptr<ImageData> create_image(const std::string& filename) override
    {
        auto surface = IMG_Load(filename.c_str());
        if ( !surface )
            throw SdlError();
        return std::make_shared<SdlImage>(surface);
    }

    std::shared_ptr<ImageData> create_image(WindowData* window) override
    {
        auto sdl_window = static_cast<SdlWindow*>(window);

        auto size = window->size();
        // pitch is the width of a row of pixels in bytes
        int pitch = size.width * 4;

        melanolib::RawBuffer data(pitch * size.height, std::nothrow);

        if ( !data )
            throw SdlError("Could not allocate buffer");

        SDL_Rect rect = { 0, 0, size.width, size.height };
        if ( SDL_RenderReadPixels(sdl_window->renderer(), &rect,
            copy_format, data.data(), pitch)
        )
            throw SdlError();

        auto surface = SDL_CreateRGBSurfaceFrom(
            data.data(),
            size.width, size.height,
            32, pitch,
            rmask, gmask, bmask, amask
        );

        if ( !surface )
            throw SdlError();

        return std::make_shared<SdlImage>(surface, std::move(data));
    }

    std::shared_ptr<FontData> create_font(
        const std::string& filename,
        std::size_t point_size,
        graphics::FontStyle style
    ) override
    {
        return std::make_shared<SdlFont>(filename.c_str(), point_size, style);
    }

private:
#   if SDL_BYTEORDER == SDL_BIG_ENDIAN
        static const Uint32 rmask = 0xff000000;
        static const Uint32 gmask = 0x00ff0000;
        static const Uint32 bmask = 0x0000ff00;
        static const Uint32 amask = 0x000000ff;
        static const Uint32 copy_format = SDL_PIXELFORMAT_RGBA8888;
#   else
        static const Uint32 rmask = 0x000000ff;
        static const Uint32 gmask = 0x0000ff00;
        static const Uint32 bmask = 0x00ff0000;
        static const Uint32 amask = 0xff000000;
        static const Uint32 copy_format = SDL_PIXELFORMAT_ABGR8888;
#   endif

    SdlSubSystem sdl{SDL_INIT_VIDEO};
};

} // namespace sdl
} // namespace backend
#endif // MELANOENGINE_BACKEND_SDL_GRAPHICS_HPP
