/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_SDL_HPP
#define MELANOENGINE_BACKEND_SDL_HPP

#include "sdl_error.hpp"
#include <memory>

namespace backend {
namespace sdl {

/**
 * \brief SDL subsystem RAII object
 */
class SdlSubSystem
{

public:
    explicit SdlSubSystem(Uint32 flags)
    {
        impl->init(flags);
    }

private:
    /**
     * \brief Shared SDL RAII object to quit when there are no backends using SDL
     */
    class Impl
    {
    public:
        Impl(){}

        ~Impl()
        {
            SDL_Quit();
        }

        /**
         * \brief Initializes the given SDL sub-system
         */
        void init(Uint32 flags)
        {
            if ( SDL_InitSubSystem(flags) != 0 )
                throw SdlError();
        }

        /**
         * \brief Returns a shared instance
         */
        static std::shared_ptr<Impl> instance()
        {
            static std::weak_ptr<Impl> pointer;
            if ( !pointer.use_count() )
            {
                auto shared = std::make_shared<Impl>();
                pointer = shared;
                return shared;
            }
            return pointer.lock();
        }

    };

    SdlSubSystem(const SdlSubSystem&) = delete;
    SdlSubSystem& operator=(const SdlSubSystem&) = delete;

    std::shared_ptr<Impl> impl = Impl::instance();
};

} // namespace sdl
} // namespace backend
#endif // MELANOENGINE_BACKEND_SDL_HPP
