/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_SDL_SDL_IMAGE_HPP
#define MELANOENGINE_BACKEND_SDL_SDL_IMAGE_HPP

#include <boost/filesystem.hpp>

#include <SDL2/SDL_image.h>

#include "melanolib/data_structures/raw_buffer.hpp"

#include "backend/common/graphics/image_data.hpp"
#include "backend/sdl/sdl_error.hpp"

namespace backend{
namespace sdl{

class SdlImage : public ImageData
{
public:
    explicit SdlImage(SDL_Surface* surface, melanolib::RawBuffer owned_pixels = {})
        : _surface(surface), _owned_pixels(std::move(owned_pixels))
    {}

    ~SdlImage()
    {
        SDL_FreeSurface(_surface);
    }

    SDL_Surface* surface()
    {
        return _surface;
    }

    graphics::Size size() override
    {
        return graphics::Size(_surface->w, _surface->h);
    }

    void save(const std::string& filename) const override
    {
        std::string extension = boost::filesystem::extension(filename);
        if ( extension == ".png" )
            SdlError::wrap_call(IMG_SavePNG(_surface, filename.c_str()));
        else if ( extension == ".bmp" )
            SdlError::wrap_call(SDL_SaveBMP(_surface, filename.c_str()));
        else
            throw SdlError("File format not supported");
    }

private:
    SDL_Surface* _surface;
    melanolib::RawBuffer _owned_pixels;
};


} // namespace sdl
} // namespace backend
#endif // MELANOENGINE_BACKEND_SDL_SDL_IMAGE_HPP
