/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_BACKEND_SDL_SDL_INPUT_HPP
#define MELANOENGINE_BACKEND_SDL_SDL_INPUT_HPP

#include "sdl_backend.hpp"
#include "backend/common/input/input_backend.hpp"
#include "backend/common/input/input_data.hpp"
#include "backend/common/input/key_data.hpp"

namespace backend {
namespace sdl {

class SdlInput : public InputData
{
public:
    input::MouseStatus mouse_status(bool global) override
    {
        input::MouseStatus status;
        Uint32 buttons;
        if ( global )
            buttons = SDL_GetGlobalMouseState(&status.pos.x, &status.pos.y);
        else
            buttons = SDL_GetMouseState(&status.pos.x, &status.pos.y);
        status.buttons = buttons_from_sdl(buttons);
        return status;
    }

    std::set<input::ScanCode> keyboard_status() override
    {
        int size;
        auto keys = SDL_GetKeyboardState(&size);
        std::set<input::ScanCode> output;
        for ( int i = 0; i < size; i++)
            if ( keys[i] )
                output.insert(input::ScanCode(i));
        return output;
    }

    static input::MouseStatus::MouseButtons buttons_from_sdl(Uint32 sdl_buttons)
    {
        input::MouseStatus::MouseButtons buttons = 0;
        if ( sdl_buttons & SDL_BUTTON_LMASK )
            buttons |= input::MouseStatus::LeftButton;
        if ( sdl_buttons & SDL_BUTTON_MMASK )
            buttons |= input::MouseStatus::MiddleButton;
        if ( sdl_buttons & SDL_BUTTON_RMASK )
            buttons |= input::MouseStatus::RightButton;
        if ( sdl_buttons & SDL_BUTTON_X1MASK )
            buttons |= input::MouseStatus::X1Button;
        if ( sdl_buttons & SDL_BUTTON_X2MASK )
            buttons |= input::MouseStatus::X2Button;
        return buttons;
    }
};

class SdlKey : public KeyData
{
public:
    explicit SdlKey(SDL_Keycode key)
        : key(key)
    {}

    std::string name() const override
    {
        return SDL_GetKeyName(key);
    }

    input::ScanCode code() const override
    {
        return input::ScanCode(SDL_GetScancodeFromKey(key));
    }

    input::KeyValue value() const override
    {
        return (key & ~SDLK_SCANCODE_MASK) | input::ScanCodeMask;
    }

    bool is_down() const override
    {
        auto scancode = code();
        int size;
        auto keys = SDL_GetKeyboardState(&size);
        if ( int(scancode) > size )
            return false;
        return keys[int(scancode)];
    }

private:
    SDL_Keycode key;
};

class SdlInputBackend : public InputBackend
{
public:
    std::unique_ptr<InputData> create_input_status() override
    {
        return std::make_unique<SdlInput>();
    }

    void listen() override
    {
        SDL_PumpEvents();
    }

    std::unique_ptr<KeyData> create_key(input::ScanCode code) override
    {
        SDL_Keycode key = SDL_GetKeyFromScancode(SDL_Scancode(code));
        if ( !key )
            throw SdlError();
        return std::make_unique<SdlKey>(key);
    }


    std::unique_ptr<KeyData> create_key(const std::string& key_name) override
    {
        SDL_Keycode key = SDL_GetKeyFromName(key_name.c_str());
        if ( !key )
            throw SdlError();
        return std::make_unique<SdlKey>(key);
    }

    std::unique_ptr<KeyData> create_key(input::KeyValue key) override
    {
        SDL_Keycode sdl_key = (key & ~input::ScanCodeMask) | SDLK_SCANCODE_MASK;
        if ( !SDL_GetScancodeFromKey(sdl_key) )
            throw SdlError();
        return std::make_unique<SdlKey>(sdl_key);
    }

private:
    SdlSubSystem sdl{SDL_INIT_EVENTS};
};

} // namespace sdl
} // namespace backend
#endif // MELANOENGINE_BACKEND_SDL_SDL_INPUT_HPP
