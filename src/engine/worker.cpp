/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "worker.hpp"
#include "engine/error.hpp"
#include "engine/log.hpp"

namespace engine{


void Worker::push(const AsyncRequestCallback& callback)
{
    schedule(callback, std::chrono::seconds(0));
}

void Worker::schedule(const AsyncRequestCallback& callback, std::chrono::milliseconds seconds)
{
    auto lock = melanolib::make_lock(mutex);
    scheduled_sleep = seconds;
    queue.push(callback);
    condition.notify_one();
}

void Worker::stop()
{
    logging::debug(worker_name) << "Stopping";
    auto lock = melanolib::make_lock(mutex);
    stopped = true;
    condition.notify_all();
}

void Worker::run()
{
    auto lock = melanolib::make_lock(mutex);
    working = true;

    bool ran = false;

    if ( !stopped )
    {
        logging::debug(worker_name) << "Starting on thread "
            << std::this_thread::get_id();
        ran = true;
    }

    while ( !stopped )
    {
        working = true;
        while ( !queue.empty() )
        {
            if ( scheduled_sleep.count() != 0 )
            {
                auto wait_for = scheduled_sleep;
                scheduled_sleep = std::chrono::milliseconds::zero();
                condition.wait_for(lock, wait_for);
            }

            auto callback = queue.front();
            queue.pop();

            lock.unlock();
            try
            {
                callback();
            }
            catch ( const Error& error )
            {
                logging::LogStream log(logging::Verbosity::Critical, worker_name);
                log << "Caught exception";
                if ( !error.origin().empty() )
                    log << " from " << error.origin();
                log << ": " << error.what();
                stop();
                return;
            }
            catch ( const std::exception& error )
            {
                logging::critical(worker_name) << "Caught exception: "
                    << error.what();
                stop();
                return;
            }

            lock.lock();

            if ( stopped )
                return;
        }
        working = false;

        condition.wait(lock);

    }

    if ( ran )
    {
        logging::debug(worker_name) << "Terminated on thread "
            << std::this_thread::get_id();
    }
    else
    {
        logging::debug(worker_name) << "Skipped execution on thead "
            << std::this_thread::get_id();
    }
}

void Worker::run_on_threads(std::size_t n_threads)
{
    auto lock = melanolib::make_lock(threads_mutex);
    if ( !threads.empty() )
    {
        throw Error("Worker already running", worker_name);
    }

    logging::debug(worker_name) << "Starting " << n_threads << " threads";
    threads = std::vector<std::thread>(n_threads);
    for ( auto& thread : threads )
        thread = std::thread([this]{ run(); });
}

void Worker::join()
{
    auto lock = melanolib::make_lock(threads_mutex);
    for ( auto& thread : threads )
        thread.join();
    threads.clear();
}

void Worker::stop_and_join()
{
    stop();
    join();
}

bool Worker::has_work()
{
    auto lock = melanolib::make_lock(mutex);
    return !stopped && (working || !queue.empty());
}


std::string Worker::name() const
{
    // it's read only so no locking needed
    return worker_name;
}

} // namespace engine
