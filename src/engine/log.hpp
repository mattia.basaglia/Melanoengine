/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_ENGINE_LOG_HPP
#define MELANOENGINE_ENGINE_LOG_HPP

#include <iostream>
#include <mutex>
#include <algorithm>
#include <atomic>
#include <array>
#include <vector>
#include <sstream>

#include "melanolib/utils/singleton.hpp"
#include "melanolib/string/stringutils.hpp"

/**
 * \note This should be called "log", blame the C math library
 */
namespace logging {

/**
 * \brief Message verbosity level
 */
enum class Verbosity
{
    Critical,   ///< An unrecoverable error that causes the engine to crash
    Error,      ///< An error that might cause the engine to malfunction
    Warning,    ///< Something is not as expected but most functionality is intact
    Info,       ///< Misc information
    Debug,      ///< Extra verbose information
};

/**
 * \brief Log singpleton object
 */
class Logger : public melanolib::Singleton<Logger>
{
public:

    /**
     * \brief Converts a string represantation of a verbosity level to an enum value
     */
    Verbosity verbosity_from_string(std::string level)
    {
        std::transform(level.begin(), level.end(), level.begin(),
                       melanolib::string::ascii::to_lower);
        for ( const auto& log : logs )
        {
            if ( level == log.name )
                return log.verbosity;
        }
        /// \todo report error
        return Verbosity::Info;
    }

    /**
     * \brief Converts a verbosity level to a string (by name).
     */
    std::string verbosity_to_string(Verbosity level)
    {
        return get_attributes(level).name;
    }

    /**
     * \brief Sets verbosity level, messages with higher verbosity won't be displayed
     */
    void set_verbosity(Verbosity verbosity)
    {
        _verbosity = verbosity;
    }

    /**
     * \brief Verbosity level
     */
    Verbosity verbosity() const
    {
        return _verbosity;
    }

    /**
     * \brief Prints all the given lines
     */
    void log(Verbosity verbosity, const std::string& origin, const std::vector<std::string>& lines);

    /**
     * \brief Available verbosity names
     */
    std::vector<std::string> verbosity_names() const;

private:
    struct LogAttributes
    {
        Verbosity   verbosity;
        const char* name;
        const char* prefix;
        const char* color;
        std::ostream& stream;
    };

    const LogAttributes& get_attributes(Verbosity verbosity)
    {
        for ( const auto& attrs : logs )
        {
            if ( attrs.verbosity == verbosity )
                return attrs;
        }

        return logs[3]; // Info by default
    }

    std::string build_prefix(const LogAttributes& attrs, const std::string& origin);

    Logger(){}
    friend ParentSingleton;

    std::atomic<Verbosity> _verbosity{Verbosity::Info};
    std::atomic<bool> _show_colors{true};

    std::mutex mutex;

    const char* clear_color = "\x1b[m";

    std::array<LogAttributes, 5> logs {{
        LogAttributes{Verbosity::Critical, "critical", "Error: ",   "\x1b[31;1m", std::cerr},
        LogAttributes{Verbosity::Error,    "error",    "Error: ",   "\x1b[31m",   std::cerr},
        LogAttributes{Verbosity::Warning,  "warning",  "Warning: ", "\x1b[33m",   std::cerr},
        LogAttributes{Verbosity::Info,     "info",     "",          "",           std::cout},
        LogAttributes{Verbosity::Debug,    "debug",    "Debug: ",   "\x1b[36m",   std::cerr},
    }};
};

/**
 * \brief Stream-like interface to Logger
 */
class LogStream
{
public:
    LogStream(Verbosity level, const std::string& origin)
        : print_out(Logger::instance().verbosity() >= level),
          level(level),
          origin(origin)
    {}

    ~LogStream()
    {
        if ( !print_out )
            return;
        Logger::instance().log(
            level,
            origin,
            melanolib::string::char_split(output.str(), '\n', false)
        );
    }

    template<class T>
        LogStream& operator<<(T&& value)
        {
            if ( print_out )
                output << std::forward<T>(value);
            return *this;
        }

private:
    bool print_out;
    std::ostringstream output;
    Verbosity level;
    std::string origin;
};

inline LogStream critical(const std::string& origin = "")
{
    return {Verbosity::Critical, origin};
}

inline LogStream error(const std::string& origin = "")
{
    return {Verbosity::Error, origin};
}

inline LogStream warning(const std::string& origin = "")
{
    return {Verbosity::Warning, origin};
}

inline LogStream info(const std::string& origin = "")
{
    return {Verbosity::Info, origin};
}

inline LogStream debug(const std::string& origin = "")
{
    return {Verbosity::Debug, origin};
}

} // namespace log
#endif // MELANOENGINE_ENGINE_LOG_HPP
