/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_ENGINE_WORKER_HPP
#define MELANOENGINE_ENGINE_WORKER_HPP

#include <queue>
#include <typeindex>
#include <functional>
#include <chrono>

#include "melanolib/utils/concurrency.hpp"

namespace engine{


class Engine;

/**
 * \brief Wraps a request sent to the engine
 */
using AsyncRequestCallback = std::function<void ()>;

/**
 * \brief Erases the type of pointers to member functions of Engine
 * and provides comparison operations suitable for storace in a map.
 */
class ErasedMethod
{
private:
    class HolderBase
    {
    public:
        virtual ~HolderBase(){}
        virtual const std::type_index& type() const noexcept = 0;
        virtual bool matches(const HolderBase& other) const noexcept = 0;
        virtual bool less_than(const HolderBase& other) const noexcept = 0;
        virtual std::unique_ptr<HolderBase> clone() const = 0;

        template<class HolderT>
        const HolderT& cast_to() const noexcept
        {
            return static_cast<const HolderT&>(*this);
        }
    };

    template<class Method>
    class Holder : public HolderBase
    {
    public:
        Holder(Method pointer) : pointer(pointer) {}

        const std::type_index& type() const noexcept override
        {
            return index;
        }

        bool matches(const HolderBase& other) const noexcept override
        {
            if ( other.type() != type() )
                return false;
            return other.cast_to<Holder>().pointer == pointer;
        }

        bool less_than(const HolderBase& other) const noexcept override
        {
            if ( other.type() != type() )
                return type() < other.type();
            return index < other.cast_to<Holder>().index;
        }

        std::unique_ptr<HolderBase> clone() const override
        {
            return std::make_unique<Holder>(pointer);
        }

        Method pointer;
        std::type_index index = typeid(Method);
    };

    template<class Ret, class... Args>
    using MethodPointer = Ret (Engine::*)(Args...);

    template<class Ret, class... Args>
    using ConstMethodPointer = Ret (Engine::*)(Args...) const;

public:
    template<class Ret, class... Args>
    explicit ErasedMethod(MethodPointer<Ret, Args...> ptr)
        : holder(std::make_unique<Holder<MethodPointer<Ret, Args...>>>(ptr))
    {}

    template<class Ret, class... Args>
    explicit ErasedMethod(ConstMethodPointer<Ret, Args...> ptr)
        : holder(std::make_unique<Holder<ConstMethodPointer<Ret, Args...>>>(ptr))
    {}

    ErasedMethod(const ErasedMethod& oth)
        : holder(oth.holder->clone()) {}
    ErasedMethod(ErasedMethod&&) = default;


    ErasedMethod& operator=(const ErasedMethod& oth)
    {
        holder = holder->clone();
        return *this;
    }
    ErasedMethod& operator=(ErasedMethod&&) = default;

    bool operator==(const ErasedMethod& other) const
    {
        return holder->matches(*other.holder);
    }

    bool operator<(const ErasedMethod& other) const
    {
        return holder->less_than(*other.holder);
    }

private:
    std::unique_ptr<HolderBase> holder;
};

/**
 * \brief Runs a queue of asynchronous events
 */
class Worker
{
public:
    Worker(std::string name)
        : worker_name(std::move(name)) {}

    ~Worker()
    {
        stop_and_join();
    }

    /**
     * \brief Adds a callback to the queue
     */
    void push(const AsyncRequestCallback& callback);

    /**
     * \brief Schedules a callback to be executed with a delay.
     *
     * It might be executed sooner if a another callback is pushed directly or
     * scheduled for a smaller interval
     */
    void schedule(const AsyncRequestCallback& callback, std::chrono::milliseconds seconds);

    /**
     * \brief Sends a request to stop the worker
     */
    void stop();

    /**
     * \brief Runs the worker synchronously
     */
    void run();

    /**
     * \brief Starts \p n_threads threads running the worker
     */
    void run_on_threads(std::size_t n_threads=1);

    /**
     * \brief Waits until all threads have finished
     */
    void join();

    /**
     * \brief same as calling stop() and join()
     */
    void stop_and_join();

    /**
     * \brief Whether the worker still has unfinished/scheduled jobs
     */
    bool has_work();

    /**
     * \brief Worker's name
     */
    std::string name() const;

private:
    std::mutex mutex;
    std::mutex threads_mutex;
    std::queue<AsyncRequestCallback> queue;
    std::condition_variable condition;
    bool stopped = false;
    std::vector<std::thread> threads;
    std::string worker_name;
    bool working = false;
    std::chrono::milliseconds scheduled_sleep{0};
};

} // namespace engine
#endif // MELANOENGINE_ENGINE_WORKER_HPP
