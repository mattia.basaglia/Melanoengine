# Copyright 2016-2017 Mattia Basaglia
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set(SOURCES
log.cpp
data.cpp
engine.cpp
worker.cpp
version.cpp
)

component_add(engine ${SOURCES})

# Melanolibs
component_link(engine melano_stringutils melano_color)

# Boost
find_package(Boost COMPONENTS filesystem REQUIRED)
component_link(engine ${Boost_LIBRARIES})
include_directories(${Boost_INCLUDE_DIRS})

# find_package(Threads REQUIRED)
# component_link(engine ${CMAKE_THREAD_LIBS_INIT})

# Configured files
configure_file(config.in.hpp config.hpp)
include_directories("${CMAKE_CURRENT_BINARY_DIR}")
