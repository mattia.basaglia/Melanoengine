/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "data.hpp"

#include <cstdlib>

#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>

#include "engine/log.hpp"
#include "engine/version.hpp"
#include "engine/error.hpp"

namespace engine {

void Data::load_paths(const std::string& executable_name)
{
    if ( !executable_name.empty() )
    {
        boost::filesystem::path exec_path = executable_name;
        auto data_path = exec_path.parent_path().parent_path() / "share" / version::short_name;
        boost::system::error_code err;
        data_path = boost::filesystem::canonical(data_path, err);
        if ( !err )
            add_path(data_path);
    }

    boost::filesystem::path home = std::getenv("HOME");
    if ( !home.empty() )
        add_writable_path(home / ".config" / version::short_name);
}

void Data::add_path(const boost::filesystem::path& path)
{
    auto absolute = boost::filesystem::absolute(path);
    paths.push_back(absolute);
    logging::debug("Data") << "Search path: " << absolute;
}

void Data::add_writable_path(const boost::filesystem::path& path, bool create_missing)
{
    auto absolute = boost::filesystem::absolute(path);
    paths.push_back(absolute);
    writable_paths.push_back(absolute);

    if ( create_missing && !boost::filesystem::exists(path) )
    {
        boost::filesystem::create_directories(absolute);
    }

    logging::debug("Data") << "Writable path: " << absolute;
}

std::string Data::readable_path(const boost::filesystem::path& name) const
{
    using namespace boost::filesystem;

    boost::system::error_code err;
    if ( exists(name, err) )
    {
        /// \todo check is readable
        return name.string();
    }

    for ( auto it = paths.rbegin(); it != paths.rend(); ++it )
    {
        boost::system::error_code err;
        path abspath = canonical(*it / name, err);
        if ( !err && is_regular_file(abspath, err) && !err )
            return abspath.string();
    }

    return "";
}

std::string Data::require_readable_path(const boost::filesystem::path& name) const
{
    std::string path = readable_path(name);
    if ( path.empty() )
        throw engine::Error("Data file not found: " + name.string(), "Game");
    return path;
}

std::string Data::writable_path(const boost::filesystem::path& name, bool create_if_missing) const
{
    using namespace boost::filesystem;

    boost::system::error_code err;
    if ( exists(name, err) )
    {
        /// \todo check is writable
        return name.string();
    }

    for ( auto it = paths.rbegin(); it != paths.rend(); ++it )
    {
        boost::system::error_code err;
        path abspath = absolute(*it / name);

        if ( !exists(abspath.parent_path(), err) && !err )
        {
            create_directories(abspath.parent_path(), err);
            if ( err )
                continue;
        }

        if ( create_if_missing && !exists(abspath, err) && !err )
        {
            ofstream file(abspath);
            // could not create it
            if ( !exists(abspath, err) || err )
                continue;
        }

        return abspath.string();
    }

    if ( create_if_missing )
        logging::warning("Data") << "Could not create file: " << name;
    return "";
}

} // namespace engine
