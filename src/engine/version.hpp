/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_VERSION_HPP
#define MELANOENGINE_VERSION_HPP

#include <string>


namespace engine {

/**
 * \brief Namespace with version information
 */
namespace version {

extern std::string name;            ///< Nice name of this engine
extern std::string short_name;      ///< Machine name of this engine
extern std::string version;         ///< Nice version number (eg: 1.0)
extern std::string version_verbose; ///< A more descriptive version number with development information
extern std::string website;         ///< Home page for the engine
extern std::string license_text;    ///< Medium-length license description

} // namespace version
} // namespace engine
#endif // MELANOENGINE_VERSION_HPP
