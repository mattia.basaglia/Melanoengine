/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#ifndef MELANOENGINE_ENGINE_CONFIG_HPP
#define MELANOENGINE_ENGINE_CONFIG_HPP

#define PROJECT_NAME            "@PROJECT_NAME@"
#define PROJECT_VERSION         "@PROJECT_VERSION@"
#define PROJECT_SHORTNAME       "@EXECUTABLE_NAME@"
#define PROJECT_WEBSITE         "@PROJECT_WEBSITE@"
#define PROJECT_DEV_VERSION     "@PROJECT_DEV_VERSION@"
#define PROJECT_BUILD_SUFFIX    "@PROJECT_BUILD_SUFFIX@"

#endif // MELANOENGINE_ENGINE_CONFIG_HPP
