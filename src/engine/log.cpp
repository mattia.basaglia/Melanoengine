/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "log.hpp"

namespace logging {

void Logger::log(Verbosity verbosity, const std::string& origin, const std::vector<std::string>& lines)
{
    if ( verbosity > _verbosity )
        return;

    std::lock_guard<std::mutex> lock(mutex);

    const LogAttributes& attrs = get_attributes(verbosity);
    auto prefix = build_prefix(attrs, origin);

    for ( const auto& line : lines )
    {
        attrs.stream << prefix << line << '\n';
    }
}

std::string Logger::build_prefix(const LogAttributes& attrs, const std::string& origin)
{
    std::string prefix;

    if ( !origin.empty() )
    {
        prefix.append(origin);
        prefix.append(": ");
    }

    if ( _show_colors )
        prefix.append(attrs.color);
    prefix.append(attrs.prefix);
    if ( _show_colors )
        prefix.append(clear_color);

    return prefix;
}

std::vector<std::string> Logger::verbosity_names() const
{
    std::vector<std::string> names;
    names.reserve(logs.size());
    for ( const auto& attr : logs )
        names.push_back(attr.name);
    return names;
}


} // namespace logging
