/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_ENGINE_ERROR_HPP
#define MELANOENGINE_ENGINE_ERROR_HPP

#include <stdexcept>

namespace engine {

/**
 * \brief Generic class for back-end errors.
 */
class Error : public std::runtime_error
{
public:
    Error(const std::string& message, const std::string& origin)
        : runtime_error(message), _origin(origin)
    {}

    const std::string& origin() const
    {
        return _origin;
    }

private:
    std::string _origin;
};


} // namespace engine
#endif // MELANOENGINE_ENGINE_ERROR_HPP
