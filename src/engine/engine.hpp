/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_ENGINE_HPP
#define MELANOENGINE_ENGINE_HPP

#include <map>

#include "engine/worker.hpp"
#include "engine/log.hpp"

namespace scripting {
class Interpreter;
} // namespace scripting

namespace engine {

/**
 * \brief Engine main class
 */
class Engine : public melanolib::Singleton<Engine>
{
public:
    /**
     * \brief Runs the engine in the current thread
     */
    void run();

    void run_script(const std::string& text);
    void run_script_file(const std::string& data_file);

    /**
     * \brief Stops all subsystems started by the engine and makes run() return
     */
    void stop();

    /**
     * \brief Queues an asyncronous request for the given method call
     */
    template<class... Params, class... Args>
    void async(void (Engine::*mem_func)(Params...), Args... args)
    {
        worker_for(ErasedMethod(mem_func)).push(
            [mem_func, args...](){
                (instance().*mem_func)(args...);
            }
        );
    }

    /**
     * \brief Delay between watchdog checks
     */
    std::chrono::seconds watchdog_delay() const;

    /**
     * \brief Change the interval between watchdog checks
     * \note Will take into effect starting from the following check
     */
    void set_watchdog_delay(std::chrono::seconds duration);

private:
    Engine(){}
    friend ParentSingleton;

    /**
     * \brief Maps methods to async workers
     */
    Worker& worker_for(const ErasedMethod& erased);

    /**
     * \brief Checks if workers have work to do, if they don't it triggers a stop()
     */
    void quit_if_no_work();

    /**
     * \brief Ensures input events are being handled
     */
    void poll_input();

    Worker main{"main worker"};
    Worker script{"script worker"};
    Worker watchdog{"watchdog"};
    std::chrono::seconds watchdog_timing{2};
    std::chrono::milliseconds input_timing{66};

    mutable std::mutex mutex;

    static const std::map<ErasedMethod, Worker Engine::*> methods_to_worker;
};

} // namespace engine

#endif // MELANOENGINE_ENGINE_HPP
