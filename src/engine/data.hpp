/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_ENGINE_DATA_HPP
#define MELANOENGINE_ENGINE_DATA_HPP

#include <string>
#include <vector>

#include <boost/filesystem.hpp>

#include "melanolib/utils/singleton.hpp"

namespace engine {

class Data : public melanolib::Singleton<Data>
{
public:
    /**
     * \brief Returns the path to a readable data file, following path priorities
     * \returns An empty string if such file does not exist
     */
    std::string readable_path(const boost::filesystem::path& name) const;

    /**
     * \brief Returns the path to a readable data file, following path priorities
     * \throws engine::Error if the file has not been found
     */
    std::string require_readable_path(const boost::filesystem::path& name) const;

    /**
     * \brief Returns the path to a writable data file
     * \returns An empty string if such file does not exist
     * \note If \p create_if_missing is \b false, this might return the path to
     * a non-existing file if the file itself does not exist but it can be created
     */
    std::string writable_path(const boost::filesystem::path& name, bool create_if_missing = false) const;

    /**
     * \brief Loads the default paths
     * \param executable_name argv[0] in main()
     */
    void load_paths(const std::string& executable_name);

    /**
     * \brief Adds a search path
     * \param path          Path to add
     * \param use_canonical If \b true, the canonical path is resolved before
     *                      adding it (which means it must exist).
     */
    void add_path(const boost::filesystem::path& path);

    /**
     * \brief Adds a search path for writable files
     * \param path           Path to add
     * \param create_missing Attempt to create missing paths
     */
    void add_writable_path(const boost::filesystem::path& path, bool create_missing = false);

private:
    Data(){}
    friend ParentSingleton;

    std::vector<boost::filesystem::path> paths;
    std::vector<boost::filesystem::path> writable_paths;
};

} // namespace engine
#endif // MELANOENGINE_ENGINE_DATA_HPP
