/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "engine/engine.hpp"
#include "scripting/interpreter.hpp"
#include "engine/data.hpp"
#include "backend/common/types.hpp"
#include "backend/common/backend.hpp"

namespace engine {

void Engine::run_script(const std::string& text)
{
    scripting::Interpreter().exec(
        text,
        scripting::ScriptIo().output_log().error_to_out()
    );
}

void Engine::run_script_file(const std::string& data_file)
{
    std::string path = Data::instance().readable_path(data_file);
    if ( path.empty() )
    {
        logging::error("Engine") << "Could not execute script file: " << data_file;
        return;
    }

    logging::debug("Engine") << "Running " << path;

    scripting::Interpreter().exec_file(
        path,
        scripting::ScriptIo().output_log().error_to_out()
    );
}

void Engine::stop()
{
    main.stop();
    script.stop();
    watchdog.stop();
}

void Engine::run()
{
    backend::Backend::instance().require_initialized();

    logging::debug("Engine") << "Started";

    scripting::Interpreter::enable_threading();
    script.run_on_threads();

    watchdog.run_on_threads();
    quit_if_no_work();

    /// \todo separate input thread (which must be the main thread for SDL)
    ///       from the message queue thread (async())
    poll_input();
    main.run();

    watchdog.join();

    scripting::Interpreter::stop();
    script.join();

    logging::debug("Engine") << "Stopped";
}

const std::map<ErasedMethod, Worker Engine::*> Engine::methods_to_worker {
    {ErasedMethod(&Engine::stop), &Engine::main},
    {ErasedMethod(&Engine::run_script), &Engine::script},
    {ErasedMethod(&Engine::run_script_file), &Engine::script},
};

Worker& Engine::worker_for(const ErasedMethod& erased)
{
    auto iter = methods_to_worker.find(erased);
    if ( iter != methods_to_worker.end() )
        return this->*(iter->second);
    logging::error("Engine") << "called unregistered async method";
    return main;
}

void Engine::quit_if_no_work()
{
    if ( /*!main.has_work() &&*/ !script.has_work() )
    {
        if ( logging::Logger::instance().verbosity() >= logging::Verbosity::Info )
        {
            auto lock = melanolib::make_lock(mutex);
            logging::info(watchdog.name()) << "Stopping engine after "
                << watchdog_timing.count() << " seconds of inactivity";
        }

        stop();
    }
    else
    {
        auto lock = melanolib::make_lock(mutex);
        watchdog.schedule(
            [](){instance().quit_if_no_work();},
            watchdog_timing
        );
    }
}

std::chrono::seconds Engine::watchdog_delay() const
{
    auto lock = melanolib::make_lock(mutex);
    return watchdog_timing;
}

void Engine::set_watchdog_delay(std::chrono::seconds duration)
{
    auto lock = melanolib::make_lock(mutex);
    watchdog_timing = duration;
}

void Engine::poll_input()
{
    backend::Backend::instance().input().listen();
    main.schedule(
        [](){instance().poll_input();},
        input_timing
    );
}

} // namespace engine
