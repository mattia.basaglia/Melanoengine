/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef SCRIPTING_NAMESPACES_PUBLIC_HPP
#define SCRIPTING_NAMESPACES_PUBLIC_HPP

namespace scripting{
namespace namespaces{

/**
 * \brief Calls PyImport_AppendInittab for the defined namespaces
 */
void init_namespaces();

} // namespace namespaces
} // namespace scripting
#endif // SCRIPTING_NAMESPACES_PUBLIC_HPP
