/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_SCRIPTING_NAMESPACES_ASSET_MANAGER_CPP
#define MELANOENGINE_SCRIPTING_NAMESPACES_ASSET_MANAGER_CPP

#include "all.hpp"

#include "game/asset_manager.hpp"
#include "graphics/image.hpp"
#include "graphics/font.hpp"

namespace scripting{
namespace namespaces{

using namespace boost::python;

template<class T>
void register_asset_registry(const char* name)
{
    using Registry = game::AssetManager::AssetRegistry<T>;
    class_<Registry, boost::noncopyable>(name, no_init)
        .def("__contains__", &Registry::contains)
        .def("__getitem__", &Registry::get)
        .def("get", &Registry::get)
        .def("__setitem__", &Registry::add)
        .def("__delitem__", &Registry::remove)
        .def("__len__", &Registry::size)
        .def("clear", &Registry::clear)
    ;
}

void asset_manager()
{
    class_<game::AssetManager, boost::noncopyable>("AssetManager")
        .def("image", &game::AssetManager::image)
        .add_property("images_max", &game::AssetManager::images_max,
                      &game::AssetManager::set_images_max)
        .add_property("images", &game::AssetManager::images)
        .add_property("images_all", &game::AssetManager::images_all)
        .def("font", &game::AssetManager::font)
        .add_property("fonts_max", &game::AssetManager::fonts_max,
                      &game::AssetManager::set_fonts_max)
        .add_property("fonts", &game::AssetManager::fonts)
        .add_property("fonts_all", &game::AssetManager::fonts_all)
        .add_property(
            "framesets",
            make_function(
                &game::AssetManager::framesets,
                return_internal_reference<>()
        ))
        .add_property(
            "animations",
            make_function(
                &game::AssetManager::animations,
                return_internal_reference<>()
        ))
    ;
    register_asset_registry<graphics::FrameSet>("FrameSetRegistry");
    register_asset_registry<graphics::Animation>("AnimationRegistry");

}

} // namespace namespaces
} // namespace scripting
#endif // MELANOENGINE_SCRIPTING_NAMESPACES_ASSET_MANAGER_CPP
