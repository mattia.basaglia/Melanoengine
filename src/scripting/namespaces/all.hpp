/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_SCRIPTING_NAMESPACES_UTILITIES_HPP
#define MELANOENGINE_SCRIPTING_NAMESPACES_UTILITIES_HPP

#include <string>
#include "scripting/boost_python_lambda.hpp"

namespace scripting {
namespace namespaces {

using namespace boost::python;

/**
 * \brief Creates a module object
 */
inline object make_module(const std::string& name)
{
    return object(borrowed(PyImport_AddModule(name.c_str())));
}

/**
 * \brief Creates a submodule of the given parent
 */
inline object make_submodule(scope& parent, const std::string& name)
{
    std::string parent_name(extract<std::string>(parent.attr("__name__")));
    return parent.attr(name.c_str()) = make_module(parent_name + '.' + name);
}

/**
 * \brief Simple interface to map C++ types to existing python classes
 * \tparam Implementer Class implementing the actual conversion functions
 * \tparam Type        C++ type to convert to/from Python
 *
 * \p Implementer must define the following static methods:
 * \code
 * // Returns the python class as an object
 * static object python_type();
 * // Returns an instance of python_type()
 * static object python_ctor(const object& type, const CppType& val);
 * // Returns a C++ object from the python value (guaranteed to be an instance of python_type()
 * static CppType cpp_ctor(const object& value);
 * \endcode
 */
template<class Implementer, class Type>
class TypeMapper
{
public:
    /// C++ type
    using CppType = Type;

    /**
     * \brief Registers the conversions
     */
    TypeMapper()
    {
        converter::registry::push_back(&convertible, &construct, boost::python::type_id<CppType>());
        to_python_converter<CppType, Implementer>();
    }

/// Follows boost boilerplate
    /**
     * \brief Performs C++ to python conversion
     */
    static PyObject* convert(const CppType& val)
    {
        object value = Implementer::python_ctor(Implementer::python_type(), val);
        return incref(value.ptr());
    }

private:
    /**
     * \brief Checks if a python object can be converted to C++
     */
    static void* convertible(PyObject* pyobj)
    {
        object type = Implementer::python_type();
        if ( PyObject_IsInstance(pyobj, type.ptr()) )
            return pyobj;
        return nullptr;
    }

    /**
     * \brief Converts a python object to C++
     */
    static void construct(
        PyObject* pyobj,
        converter::rvalue_from_python_stage1_data* data
    )
    {
        void* buffer = ((converter::rvalue_from_python_storage<CppType>*)(data))->storage.bytes;

        new (buffer) CppType(Implementer::cpp_ctor(object(borrowed(pyobj))));

        data->convertible = buffer;
    }
};


/**
 * \brief Simple interface to create custom implicit conversion
 * \tparam Implementer Class implementing the actual conversion functions
 * \tparam Source      C++ type to convert from
 * \tparam Target      C++ type to convert into
 *
 * \p Implementer must define the following static method:
 * \code
 * // Returns the converted object
 * static Target convert(Source& source);
 * \endcode
 */
template<class Implementer, class Source, class Target>
class CustomImplicitConversion
{
public:
/// Follows boost boilerplate, mostly taken from implicitly_convertible
    CustomImplicitConversion()
    {
        converter::registry::push_back(
            &convertible,
            &construct,
            boost::python::type_id<Target>(),
            &converter::expected_from_python_type_direct<Source>::get_pytype
        );
    }

private:
    static void* convertible(PyObject* obj)
    {
        bool can_convert = converter::implicit_rvalue_convertible_from_python(
            obj,
            converter::registered<Source>::converters
        );
        return can_convert ? obj : nullptr;
    }

    static void construct(PyObject* obj, converter::rvalue_from_python_stage1_data* data)
    {
        void* storage = ((converter::rvalue_from_python_storage<Target>*)data)->storage.bytes;

        arg_from_python<Source> get_source(obj);
        bool convertible = get_source.convertible();
        BOOST_VERIFY(convertible);

        new (storage) Target(Implementer::convert(get_source()));

        // record successful construction
        data->convertible = storage;
    }
};

/**
 * \brief Exposes an automatic conversion between references to shared pointers
 * using shared_from_this
 */
template<class Source, class Target=std::shared_ptr<Source>>
struct ObjectToSharedConversion : public CustomImplicitConversion<ObjectToSharedConversion<Source, Target>, Source, Target>
{
    static Target convert(Source& source)
    {
        return source.shared_from_this();
    }
};

/**
 * \brief Defines symbols in `melanoengine.geo`
 */
void melanoengine_geo(scope& parent);

/**
 * \brief Defines symbols in `melanoengine.graphics`
 */
void melanoengine_graphics(scope& parent);

/**
 * \brief Defines symbols in `melanoengine.input`
 */
void melanoengine_input(scope& parent);

} // namespace namespaces
} // namespace scripting
#endif // MELANOENGINE_SCRIPTING_NAMESPACES_UTILITIES_HPP
