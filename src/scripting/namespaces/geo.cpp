/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "all.hpp"

#include "melanolib/geo/geo.hpp"
#include "melanolib/string/format.hpp"

using namespace boost::python;

namespace scripting{
namespace namespaces{


template<class Scalar, class Comparator = melanolib::math::compare_equals<Scalar>>
void geo_module(scope& parent, const std::string& name)
{
    using Point = melanolib::geo::Point<Scalar, Comparator>;
    using Size = melanolib::geo::Size<Scalar, Comparator>;
    using PolarVector = melanolib::geo::PolarVector<Scalar, Comparator>;
    using Line = melanolib::geo::Line<Scalar, Comparator>;
    using Rectangle = melanolib::geo::Rectangle<Scalar, Comparator>;
    using Circle = melanolib::geo::Circle<Scalar, Comparator>;

    scope geoscope(make_submodule(parent, name));

    class_<Point>("Point")
        .def(init<Scalar, Scalar>())
        .def_readwrite("x", &Point::x)
        .def_readwrite("y", &Point::y)
        .def(self += other<Point>())
        .def(self -= other<Point>())
        .def(self *= Scalar())
        .def(self /= Scalar())
        .def(self + other<Point>())
        .def(self - other<Point>())
        .def(self * Scalar())
        .def(self / Scalar())
        .def(Scalar() * self)
        .def(+self)
        .def(-self)
        .def(self == other<Point>())
        .def(self != other<Point>())
        .def("magnitude", &Point::magnitude)
        .def("distance_to", &Point::distance_to)
        .def("__str__", [name](const Point& val) {
            return melanolib::string::format::sprintf(
                "%s.Point(%n, %n)",
                name, val.x, val.y
            );
        });

    ;
    def("distance", &melanolib::geo::distance<Scalar, Comparator>);

    class_<Size>("Size")
        .def(init<Scalar, Scalar>())
        .def_readwrite("width", &Size::width)
        .def_readwrite("height", &Size::height)
        .def(self == other<Size>())
        .def(self != other<Size>())
        .def("__str__", [name](const Size& val) {
            return melanolib::string::format::sprintf(
                "%s.Size(%n, %n)",
                name, val.width, val.height
            );
        });
    ;

    class_<PolarVector>("PolarVector")
        .def(init<Scalar, Scalar>())
        .def(init<Point>())
        .add_property("length", &PolarVector::length)
        .add_property("angle", &PolarVector::angle)
        .def(self += other<Point>())
        .def(self == other<PolarVector>())
        .def(self != other<PolarVector>())
        .add_property("point", &PolarVector::point)
        .def("__str__", [name](const PolarVector& val) {
            return melanolib::string::format::sprintf(
                "%s.PolarVector(%n, %n)",
                name, val.length, val.angle
            );
        });

    ;

    class_<Line>("Line")
        .def(init<Point, Scalar, Scalar>())
        .def(init<Point, Point>())
        .def("__init__", [](object self, Scalar x1, Scalar y1, Scalar x2, Scalar y2)
        {
            return self.attr("__init__")(
                object(Point(x1, y1)),
                object(Point(x2, y2))
            );
        })
        .def_readwrite("p1", &Line::p1)
        .def_readwrite("p2", &Line::p2)
        .add_property("dx", &Line::dx)
        .add_property("dy", &Line::dy)
        .add_property("length", &Line::length, &Line::set_length)
        .add_property("angle", &Line::angle, &Line::set_angle)
        .def("point_at", &Line::point_at)
        .def(self == other<Line>())
        .def(self != other<Line>())
        .def("__str__", [name](const Line& val) {
            return melanolib::string::format::sprintf(
                "%s.Line((%n, %n), (%n, %n))",
                name, val.p1.x, val.p1.y, val.p2.x, val.p2.y
            );
        });
    ;

    class_<Rectangle>("Rectangle")
        .def(init<Scalar, Scalar, Scalar, Scalar>())
        .def(init<Point, Size>())
        .def(init<Point, Point>())
        .def_readwrite("x", &Rectangle::x)
        .def_readwrite("y", &Rectangle::y)
        .def_readwrite("width", &Rectangle::width)
        .def_readwrite("height", &Rectangle::height)
        .add_property("top", &Rectangle::top)
        .add_property("left", &Rectangle::left)
        .add_property("right", &Rectangle::right)
        .add_property("bottom", &Rectangle::bottom)
        .add_property("top_left", &Rectangle::top_left)
        .add_property("bottom_right", &Rectangle::bottom_right)
        .add_property("top_right", &Rectangle::top_right)
        .add_property("bottom_left", &Rectangle::bottom_left)
        .add_property("center", &Rectangle::center)
        .add_property("area", &Rectangle::area)
        .add_property("size", &Rectangle::size)
        .def("contains", (bool (Rectangle::*)(const Point&) const) &Rectangle::contains)
        .def("contains", (bool (Rectangle::*)(Scalar, Scalar) const) &Rectangle::contains)
        .def("intersects", &Rectangle::intersects)
        .def("translate", (void (Rectangle::*)(const Point&)) &Rectangle::translate)
        .def("translate", (void (Rectangle::*)(Scalar, Scalar)) &Rectangle::translate)
        .def("translated", (Rectangle (Rectangle::*)(const Point&) const) &Rectangle::translated)
        .def("translated", (Rectangle (Rectangle::*)(Scalar, Scalar) const) &Rectangle::translated)
        .def("intersection", &Rectangle::intersection)
        .def("united", &Rectangle::united)
        .def("unite", &Rectangle::unite)
        .def(self |= other<Rectangle>())
        .def(self | other<Rectangle>())
        .def(self &= other<Rectangle>())
        .def(self & other<Rectangle>())
        .def("is_valid", &Rectangle::is_valid)
        .def(self == other<Rectangle>())
        .def(self != other<Rectangle>())
        .def("nearest", &Rectangle::nearest)
        .def("expand", &Rectangle::expand)
        .def("expanded", &Rectangle::expanded)
        .def("__str__", [name](const Rectangle& val) {
            return melanolib::string::format::sprintf(
                "%s.Rectangle((%n, %n), %nx%n)",
                name, val.x, val.y, val.width, val.height
            );
        });
    ;

    class_<Circle>("Circle")
        .def(init<Scalar, Scalar, Scalar>())
        .def(init<Point, Scalar>())
        .def_readwrite("origin", &Circle::origin)
        .def_readwrite("radius", &Circle::radius)
        .def("contains", (bool (Circle::*)(const Point&) const)
             &Circle::contains)
        .def("contains", (bool (Circle::*)(const Rectangle&) const)
             &Circle::contains)
        .def("intersects", (bool (Circle::*)(const Circle&) const)
             &Circle::intersects)
        .def("intersects", (bool (Circle::*)(const Rectangle&) const)
             &Circle::intersects)
        .def("__str__", [name](const Circle& val) {
            return melanolib::string::format::sprintf(
                "%s.Circle((%n, %n),%n)",
                name, val.origin.x, val.origin.y, val.radius
            );
        });
    ;
}

void melanoengine_geo(scope& parent)
{
    scope module_geo_parent(make_submodule(parent, "geo"));

    geo_module<int>(module_geo_parent, "geo_int");
    geo_module<float>(module_geo_parent, "geo_float");
}

} // namespace namespaces
} // namespace scripting
