/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "all.hpp"
#include "public.hpp"

#include "melanolib/math/math.hpp"

#include "engine/engine.hpp"
#include "engine/data.hpp"
#include "backend/common/types.hpp"
#include "backend/common/backend.hpp"

namespace scripting{
namespace namespaces{

using namespace boost::python;

/**
 * \brief Converts std::chrono::seconds to datetime.timedelta
 * \see TypeMapper
 */
struct SecondsToTimedelta : TypeMapper<SecondsToTimedelta, std::chrono::seconds>
{
    static object python_type()
    {
        return import("datetime").attr("timedelta");
    }

    static object python_ctor(const object& type, const CppType& val)
    {
        return type(0, val.count());
    }

    static CppType cpp_ctor(const object& value)
    {
        return CppType(melanolib::math::round<CppType::rep, double>(
            extract<double>(value.attr("total_seconds")())
        ));
    }
};

/**
 * \brief Converts std::chrono::milliseconds to datetime.timedelta
 * \see TypeMapper
 */
struct MillisecondsToTimedelta : TypeMapper<MillisecondsToTimedelta, std::chrono::milliseconds>
{
    static object python_type()
    {
        return import("datetime").attr("timedelta");
    }

    static object python_ctor(const object& type, const CppType& val)
    {
        return type(0, val.count() / 1000.0);
    }

    static CppType cpp_ctor(const object& value)
    {
        return CppType(melanolib::math::round<CppType::rep, double>(
            extract<double>(value.attr("total_seconds")()) * 1000
        ));
    }
};

void asset_manager();

/**
 * \brief Defines symbols in the \c melanoengine Python module
 */
BOOST_PYTHON_MODULE(melanoengine)
{
    scope module_melanoengine;

    /// \todo maybe Engine and Backend can be hidden from Python
    class_<engine::Engine, engine::Engine*, boost::noncopyable>("Engine", no_init)
        .def("stop", &engine::Engine::stop)
        .def("async_stop", [](engine::Engine* engine){
            engine->async(&engine::Engine::stop);
        })
        .add_property("watchdog_delay", &engine::Engine::watchdog_delay, &engine::Engine::set_watchdog_delay)
    ;
    module_melanoengine.attr("engine") = ptr(&engine::Engine::instance());

    class_<backend::Backend, backend::Backend*, boost::noncopyable>("Backend", no_init)
        .def("initialize", &backend::Backend::initialize)
    ;
    module_melanoengine.attr("backend") = ptr(&backend::Backend::instance());

    class_<engine::Data, engine::Data*, boost::noncopyable>("Data", no_init)
        .def("readable_path", &engine::Data::readable_path)
        .def("require_readable_path", &engine::Data::require_readable_path)
        .def("writable_path", &engine::Data::writable_path,
             (arg("name"), arg("create_if_missing")=false))
        .def("load_paths", &engine::Data::load_paths)
        .def("add_path", &engine::Data::add_path)
        .def("add_writable_path", &engine::Data::add_writable_path,
             (arg("path"), arg("create_if_missing")=false))
    ;
    module_melanoengine.attr("data") = ptr(&engine::Data::instance());
    implicitly_convertible<std::string, boost::filesystem::path>();

    asset_manager();

    SecondsToTimedelta();
    MillisecondsToTimedelta();

    melanoengine_geo(module_melanoengine);
    melanoengine_graphics(module_melanoengine);
    melanoengine_input(module_melanoengine);
}

void init_namespaces()
{
    PyImport_AppendInittab("melanoengine", &PyInit_melanoengine);
}

} // namespace namespaces
} // namespace scripting
