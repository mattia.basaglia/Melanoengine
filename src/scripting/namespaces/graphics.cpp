/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "all.hpp"
#include "graphics/window.hpp"
#include "graphics/painter.hpp"
#include "graphics/animation.hpp"

#include "melanolib/color/color.hpp"

namespace scripting{
namespace namespaces{

using namespace boost::python;
using namespace graphics;

void window()
{
    class_<Window, std::shared_ptr<Window>, boost::noncopyable>
        ("Window", "A window on the desktop environment", no_init)
        .def(init<Rectangle, optional<std::string, bool>>("Creates a window"))
        .def(init<Size, optional<std::string, bool>>("Creates a window centered on the screen"))
        .add_property("size", &Window::size, &Window::resize,
                      "Size of the window, excluding decorations")
        .add_property("pos", &Window::pos, &Window::move,
                      "Window position on the screen")
        .def("move", &Window::move,
             "Moves the window to the specified screen coordinates")
        .add_property("rect", &Window::rect,
                      "Window rectangle in screen coordinates")
        .def("move_to_center", &Window::move_to_center,
             "Ensures the window is at the center of the screen")
        .add_property("fullscreen", &Window::fullscreen, &Window::set_fullscreen,
                      "Whether the window should fill the entire screen")
        .add_property("title", &Window::title, &Window::set_title,
                      "Window title in the window manager")
    ;
}

template<class Scalar, std::size_t Size>
tuple vec_to_tuple(const melanolib::math::Vector<Scalar, Size>& vec)
{
    list components;
    for ( Scalar v : vec )
        components.append(v);
    return tuple(components);
}

template<class Scalar>
object vec_to_tuple(Scalar val)
{
    return object(val);
}

template<class Repr>
object color_to_tuple(const melanolib::color::Color& color)
{
    return vec_to_tuple(color.to<Repr>().vec());
}

template<class Repr, class Component=float, class Keywords>
object color_constructor(const Keywords& keywords)
{
    return make_constructor(
        [](Component c1, Component c2, Component c3, Component c4){
            return std::make_shared<melanolib::color::Color>(Repr(c1, c2, c3), c4);
        },
        default_call_policies(),
        keywords
    );
}

void color()
{
    using namespace melanolib::color::repr;
    class_<Color, std::shared_ptr<Color>>("Color", "Immutable color object")
        .def("__init__",
            color_constructor<HSVf>((arg("h"), arg("s"), arg("v"), arg("a")=1.0)),
            "Builds a Color object from HSV coordinates (in [0, 1])"
        )
        .def("__init__",
            color_constructor<Lab>((arg("l"), arg("a"), arg("b"), arg("alpha")=1.0)),
            "Builds a Color object from Lab coordinates"
        )
        .def("__init__",
            color_constructor<XYZ>((arg("x"), arg("y"), arg("z"), arg("a")=1.0)),
            "Builds a Color object from XYZ coordinates"
        )
        .def("__init__",
            color_constructor<RGBf>((arg("r"), arg("g"), arg("b"), arg("a")=1.0)),
            "Builds a Color object from floating-point RGB coordinates in [0, 1]"
        )
        .def("__init__",
            color_constructor<RGB, int>((arg("r"), arg("g"), arg("b"), arg("a")=255)),
            "Builds a Color object from integer RGB coordinates in [0, 255]"
        )
        .add_property("alpha", &Color::alpha, "Alpha channel, as an int in [0, 255]")
        .add_property("alpha_float", &Color::alpha_float, "Alpha channel, as a float in [0, 1]")
        .add_property("red", &Color::red, "Red component, as an int in [0, 255]")
        .add_property("green", &Color::green, "Green component, as an int in [0, 255]")
        .add_property("blue", &Color::blue, "Blue component, as an int in [0, 255]")
        .add_property("valid", &Color::valid, "Whether the color is a valid color")
        .def(self == other<Color>())
        .def(self != other<Color>())
        .def("__str__", [](const Color& val) { return val.format(); })
        .def("distance", &Color::distance, "Distance in the Lab space (as a simple 2-norm distance)")
        .def("blend", &Color::blend<RGBf>, (arg("color"), arg("factor")=0.5),
             "Linear intepolation in RGB space")
        .def("visual_blend", &Color::blend<Lab>, (arg("color"), arg("factor")=0.5),
             "Linear intepolation in CIE Lab space")
        .def("format", &Color::format, "Formats the color into a string")
        .add_property("rgb", &color_to_tuple<RGB>, "Integer RGB tuple")
        .add_property("rgb_float", &color_to_tuple<RGBf>, "Floating-point RGB tuple")
        .add_property("hsv", &color_to_tuple<HSVf>, "Floating-point HSV tuple")
        .add_property("lab", &color_to_tuple<Lab>, "CIE Lab tuple")
        .add_property("xyz", &color_to_tuple<XYZ>, "CIE XYZ tuple")
        .add_property("int24", &color_to_tuple<RGB_int24>, "RGB as a single 24 bit integer")
        .add_property("int12", &color_to_tuple<RGB_int12>, "RGB as a single 12 bit integer")
        .add_property("int3", &color_to_tuple<RGB_int3>,
                      "RGB as a single 3 bit integer, plus an extra high bit for bright colors")
    ;
}

template<class... Args>
class PainterDrawImage
{
public:
    void operator()(Painter& p, Args... args) const
    {
        p.draw_image(args...);
    }
};

void paint()
{
    enum_<BlendMode>("BlendMode", "Expresses how transparency should be handled")
        .value("NoBlend", BlendMode::NoBlend)
        .value("Alpha", BlendMode::Alpha)
        .value("Add", BlendMode::Add)
    ;

    class_<Paint>("Paint", "Paint used for drawing", no_init)
        .def(init<Color, optional<BlendMode>>())
        .def_readwrite("color", &Paint::color)
        .def_readwrite("blend_mode", &Paint::blend_mode)
    ;
    implicitly_convertible<Color, Paint>();

    class_<Painter, boost::noncopyable>
        ("Painter", "Performs drawing operations on a drawable surface", no_init)
        .def(init<Window&>())
        .def(init<Image&>())
        .def("commit", &Painter::commit,
             "Ensures all paint operations appear on the target")
        .def("fill", &Painter::fill, "Fills the target with the given color")
        .def("draw_rect", &Painter::draw_rect)
        .def("draw_line", &Painter::draw_line)
        .def("draw_pixel", &Painter::draw_pixel)
        .def("draw_image", PainterDrawImage<const Image&, const Point&>(),
             "Draws the whole image at the given location")
        .def("draw_image",
             PainterDrawImage<const Image&, const Rectangle&, const Point&>(),
             "Draws a subset of the image at the given location"
        )
        .def("draw_image",
             PainterDrawImage<const Image&, const Rectangle&, const Rectangle&>(),
             "Draws a subset of the image stretching it to fill the destination"
        )
        .def("draw_image", PainterDrawImage<const SubImage&, const Rectangle&>(),
             "Draws a subset of the image at the given location")
        .def("draw_image", PainterDrawImage<const SubImage&, const Point&>(),
             "Draws a subset of the image stretching it to fill the destination")
        .def("draw_text", &Painter::draw_text,
             "Draws the given string in the selected style")
        // Simulate RAII
        .def("__enter__", [](Painter& p){return &p;}, return_internal_reference<>())
        .def("__exit__", [](Painter& p, const object&, const object&, const object&){
            p.commit();
        }, "Calls self.commit()")
    ;
}

void image()
{
    auto flip_args = (arg("horizontal")=true, arg("vertical")=false);

    class_<Image>("Image", no_init)
        .def(init<std::string>("Opens the given file as an image"))
        .def(init<Size, Color>("Creates a new image"))
        .def(init<const Window&>("Creates a screenshot of the given window"))
        .add_property("width", &Image::width)
        .add_property("height", &Image::height)
        .add_property("size", &Image::size)
        .add_property("rect", &Image::rect)
        .def("sub_image", &Image::sub_image)
        .def("flipped", &Image::flipped, flip_args)
        .def("save", &Image::save)
        .def(self == other<Image>())
        .def(self != other<Image>())
    ;

    class_<SubImage>("SubImage", "A subsection of an image")
        .def(init<Image, optional<Rectangle, bool, bool>>())
        .add_property("width", &SubImage::width)
        .add_property("height", &SubImage::height)
        .add_property("size", &SubImage::size)
        .add_property("rect", &SubImage::rect, &SubImage::set_rect)
        .def("image", &SubImage::image,
             "Returns a reference to the underlying image",
             return_value_policy<return_by_value>()
        )
        .add_property("valid", &SubImage::valid)
        .add_property("horizontal_flip", &SubImage::horizontal_flip,
            &SubImage::set_horizontal_flip)
        .add_property("vertical_flip", &SubImage::vertical_flip,
            &SubImage::set_vertical_flip)
        .def("flip", &SubImage::flip, flip_args, "Flips the image")
        .def("set_flip", &SubImage::set_flip,
             "Set a specific flip relative to the source image")
        .def("flipped", &SubImage::flipped, flip_args)
    ;

    implicitly_convertible<Image, SubImage>();
}

struct FrameSetWrapper : FrameSet, wrapper<FrameSet>
{
    SubImage image_at(size_type index) const override
    {
        return this->get_override("image_at")(index);
    }

    size_type size() const override
    {
        return this->get_override("size")();
    }

    Size frame_size() const
    {
        return this->get_override("frame_size")();
    }
};

void frame_set()
{
    class_<FrameSetWrapper, std::shared_ptr<FrameSet>, boost::noncopyable>
        ("FrameSet", "A collection of sub-images", no_init)
        .def("image_at", &FrameSet::image_at,
             "Retrieves the image at the given location")
        .def("size", &FrameSet::size,
             "Number of images")
        .def("frame_size", &FrameSet::frame_size,
             "Size of a single frame")
    ;

    class_<FrameCollection, std::shared_ptr<FrameCollection>, bases<FrameSet>>
        ("FrameCollection", "FrameSet from a group of images", no_init)
        .def("set_image", &FrameCollection::set_image,
             "Sets an image at a specified index")
        .def("append", &FrameCollection::append,
             "Add an image generating an index automatically")
    ;

    class_<SpriteSheet, std::shared_ptr<SpriteSheet>, bases<FrameSet>>
        ("SpriteSheet", "Frame set created from a grid subdivision of a single image", no_init)
        .def(init<SubImage, Size, optional<Dimension>>())
        .def(init<Image&, Size, optional<Dimension>>())
        .def(init<SubImage, Dimension, Dimension>())
        .def("image_at_grid", &SpriteSheet::image_at_grid,
             "Returns the image at the given grid coordinates")
        .def("index_at_grid", &SpriteSheet::index_at_grid,
             "Returns the linear indef for the given grid coordinates")
    ;

    class_<Animation, std::shared_ptr<Animation>, boost::noncopyable>
        ("Animation", no_init)
        .setattr("Default", int(Animation::Default))
        .setattr("NoLoop", int(Animation::NoLoop))
        .setattr("Randomized", int(Animation::Randomized))
        .setattr("UserDefined", int(Animation::UserDefined))
        .def(init<FrameSet&, Animation::FrameIndex,
             Animation::FrameIndex, Dimension,
             optional<Animation::AnimationFlags, Point>>())
        .def("frame", &Animation::frame,
             "Get the frame image at the given index")
        .def("next_frame", &Animation::next_frame, (arg("previous"), arg("count")=1),
             "Get a relative frame in the animation")
        .def("fps", &Animation::fps,
             "Animation speed in frames per second")
        .def("frame_time", &Animation::frame_time,
             "Get the time a frame shall last for")
        .def("is_last_frame", &Animation::is_last_frame,
             "Checks whether a frame is the last of the animation")
        .add_property("offset", &Animation::offset, &Animation::set_offset,
             "The offset this animation should be rendered from the object origin")
    ;

    ObjectToSharedConversion<FrameSetWrapper, std::shared_ptr<FrameSet>>();
    ObjectToSharedConversion<FrameCollection, std::shared_ptr<FrameSet>>();
    ObjectToSharedConversion<SpriteSheet, std::shared_ptr<FrameSet>>();
    ObjectToSharedConversion<Animation>();
}

void font()
{
    class_<Font, std::shared_ptr<Font>>("Font", no_init)
        .def_readonly("Regular", FontStyle(Regular))
        .def_readonly("Bold", FontStyle(Bold))
        .def_readonly("Italic", FontStyle(Italic))
        .def_readonly("Underline", FontStyle(Unerline))
        .def(init<std::string, std::size_t, optional<Font::Style>>())
        .add_property("style", &Font::style)
        .add_property("bold", &Font::bold)
        .add_property("italic", &Font::italic)
        .add_property("underline", &Font::underline)
        .add_property("regular", &Font::regular)
        .add_property("monospace", &Font::monospace)
        .add_property("max_height", &Font::max_height)
        .add_property("ascent", &Font::ascent)
        .add_property("descent", &Font::descent)
        .add_property("line_height", &Font::line_height)
        .add_property("family", &Font::family)
        .def("has_character", [](Font& font, char c){return font.has_character(c);})
        .def("has_character", [](Font& font, uint32_t c){return font.has_character(c);})
        .def("glyph_metrics", [](Font& font, char c){return font.glyph_metrics(c);})
        .def("glyph_metrics", [](Font& font, uint32_t c){return font.glyph_metrics(c);})
        .def("size", &Font::size)
    ;

    class_<GlyphMetrics>("GlyphMetrics", no_init)
        .def_readonly("box", &GlyphMetrics::box)
        .def_readonly("advance", &GlyphMetrics::advance)
    ;

    enum_<TextVerticalAlign>("TextVerticalAlign")
        .value("Top", TextVerticalAlign::Top)
        .value("Baseline", TextVerticalAlign::Baseline)
        .value("Middle", TextVerticalAlign::Middle)
        .value("Bottom", TextVerticalAlign::Bottom)
    ;

    class_<FontPaint, bases<Paint>>("FontPaint", no_init)
        .def(init<Font, Color, optional<BlendMode, TextVerticalAlign, float>>(
            (arg("font"),
             arg("color"),
             arg("blend_mode") = BlendMode::Alpha,
             arg("vertical_align") = TextVerticalAlign::Top,
             arg("line_separation") = 1.0
            )
        ))
        .def_readwrite("font", &FontPaint::font)
        .def_readwrite("vertical_align", &FontPaint::vertical_align)
    ;
}

void melanoengine_graphics(scope& parent)
{
    scope module_graphics(make_submodule(parent, "graphics"));

    window();
    color();
    paint();
    image();
    frame_set();
    font();
}

} // namespace namespaces
} // namespace scripting
