/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_SCRIPTING_IO_PROCESSORS_HPP
#define MELANOENGINE_SCRIPTING_IO_PROCESSORS_HPP

#include "scripting/interpreter.hpp"

namespace scripting {

class OuputProcessor
{
public:
    explicit OuputProcessor(ScriptIo::Output print)
    : print(std::move(print)) {}

    void write(std::string msg)
    {
        auto newline = msg.find('\n');

        while ( newline != std::string::npos )
        {
            line += msg.substr(0, newline);
            flush();

            msg.erase(0, newline+1);
            newline = msg.find('\n');
        }

        line += msg;
    }

    void write_unicode(const boost::python::object& unicode)
    {
        using namespace boost::python;
        write(extract<std::string>(str(unicode).encode("utf-8")));
    }

    void flush()
    {
        if ( !line.empty() )
            print(line);
        line.clear();
    }

private:
    std::string line; ///< Partial line buffer
    ScriptIo::Output print; ///< Functor called when a line has to be printed
};

class InputProcessor
{
public:
    explicit InputProcessor(ScriptIo::Input input)
    : input(std::move(input)) {}

    std::string readline()
    {
        return input();
    }

private:
    ScriptIo::Input input;
};

struct ScriptIoProcessor
{
    explicit ScriptIoProcessor(const ScriptIo& io)
        : stdout(io.stdout()),
          stderr(io.stderr()),
          stdin(io.stdin()),
          io(io)
    {}

    OuputProcessor stdout;
    OuputProcessor stderr;
    InputProcessor stdin;
    const ScriptIo& io;
};

} // namespace scripting
#endif // MELANOENGINE_SCRIPTING_IO_PROCESSORS_HPP
