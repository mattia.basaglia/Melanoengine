/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "interpreter.hpp"

#include "engine/error.hpp"
#include "scripting/namespaces/public.hpp"
#include "scripting/io_processors.hpp"

namespace scripting {

bool Interpreter::patch_builtins(ScriptIoProcessor& io)
{
    using namespace boost::python;

    try {
        object sys_module = import("sys");
        sys_module.attr("stdout") = ptr(&io.stdout);
        sys_module.attr("stderr") = ptr(&io.stderr);
        sys_module.attr("stdin") = ptr(&io.stdin);

        return true;
    } catch (const error_already_set&) {
        handle_exception(io.io, false);
        logging::error("Python") << "Exception on python script initialization";
        return false;
    }
}

Interpreter::Interpreter()
{
    initialize();

    try {
        _globals["__builtins__"] = boost::python::import("__main__").attr("__builtins__");
    } catch (const boost::python::error_already_set&) {
        logging::warning("Python") << "Could not set built-ins";
    }
}

void Interpreter::exec(const std::string& python_code, const ScriptIo& io)
{
    ScriptIoProcessor processor(io);
    if ( !patch_builtins(processor) )
        return;

    try {
        boost::python::exec(python_code.c_str(), _globals);
    } catch (const boost::python::error_already_set&) {
        handle_exception(io, true);
    }
}

void Interpreter::exec_file(const std::string& file, const ScriptIo& io)
{
    ScriptIoProcessor processor(io);
    if ( !patch_builtins(processor) )
        return;

    try {
        boost::python::exec_file(file.c_str(), _globals);
    } catch (const boost::python::error_already_set&) {
        handle_exception(io, true);
    }
}

void Interpreter::handle_exception(const ScriptIo& io, bool print_trace)
{
    using namespace boost::python;

    try {
        PyObject *ptype, *pvalue, *ptraceback;
        PyErr_Fetch(&ptype, &pvalue, &ptraceback);
        PyErr_NormalizeException(&ptype, &pvalue, &ptraceback);

        if ( print_trace )
        {
            Py_INCREF(ptype);
            Py_INCREF(pvalue);
            if ( ptraceback )
                Py_INCREF(ptraceback);
        }

        handle<> exc_handle(pvalue);
        object exception(exc_handle);

        logging::warning("Python")
            << std::string(extract<std::string>(
                object(handle<>(ptype)).attr("__name__"))
            ) << ": "
            << std::string(extract<std::string>(str(exception)));

        io.exception(exception);

        if ( print_trace )
        {
            PyErr_Restore(ptype, pvalue, ptraceback);
            // Note: PyErr_Print uses stderr from the processor
            PyErr_Print();
        }

    } catch (const boost::python::error_already_set&) {
        logging::error("Python") << "Error handling python exception";
    }
}

void Interpreter::stop(const std::string& message)
{
    if ( Py_IsInitialized() )
    {
        logging::debug("Python") << "Stopping python scripts";
        PyGILState_STATE state = PyGILState_Ensure();
        PyErr_SetString(PyExc_SystemError, message.c_str());
        PyGILState_Release(state);
    }
}

void Interpreter::enable_threading()
{
    initialize();
    PyEval_InitThreads();
}

void Interpreter::initialize()
{
    using namespace boost::python;

    if ( !Py_IsInitialized() )
    {
        namespaces::init_namespaces();
        Py_Initialize();

        if ( !Py_IsInitialized() )
            throw engine::Error("Could not initialize the interpreter", "Python");

        try {
            class_<OuputProcessor>("OuputProcessor", no_init)
                    .def("write", &OuputProcessor::write)
                    .def("write", &OuputProcessor::write_unicode)
                    .def("flush", &OuputProcessor::flush)
                ;

            class_<InputProcessor>("InputProcessor", no_init)
                .def("readline", &InputProcessor::readline)
            ;

            import("melanoengine");
        } catch (const error_already_set&) {
            throw engine::Error("Could not initialize the interpreter", "Python");
        }
    }

}

} // namespace scripting
