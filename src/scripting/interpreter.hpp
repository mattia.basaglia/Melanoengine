/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_SCRIPTING_INTERPRETER_HPP
#define MELANOENGINE_SCRIPTING_INTERPRETER_HPP

#include <string>
#include <vector>
#include <functional>

#include <boost/python.hpp>

#include "engine/log.hpp"

namespace scripting {

/**
 * \brief Python terminal I/O handler
 */
class ScriptIo
{
public:
    /// Functor type for input streams
    using Input = std::function<std::string()>;
    /// Functor type for output streams
    using Output = std::function<void(const std::string&)>;
    /// Functor type for exception handling
    using Exception = std::function<void(const boost::python::object&)>;

    /**
     * \brief Returns the input functor
     */
    const Input& stdin() const
    {
        return _stdin;
    }

    /**
     * \brief Makes stdin return strings from a copy of the given vector
     */
    ScriptIo& input(const std::vector<std::string>& lines)
    {
        std::size_t index = 0;
        _stdin = [lines, index]() mutable -> std::string {
            if ( index >= lines.size() )
                return "";
            return lines[index++];
        };

        return *this;
    }

    /**
     * \brief Makes stdin return strings from the given vector reference
     */
    ScriptIo& input(std::vector<std::string>& lines)
    {
        std::size_t index = 0;
        _stdin = [&lines, index]() mutable -> std::string {
            if ( index >= lines.size() )
                return "";
            return lines[index++];
        };

        return *this;
    }

    /**
     * \brief Sets the input functor
     */
    ScriptIo& input(const Input& func)
    {
        _stdin = func;
        return *this;
    }

    /**
     * \brief Returns the output functor
     */
    const Output& stdout() const
    {
        return _stdout;
    }

    /**
     * \brief Sets the output functor
     */
    ScriptIo& output(const Output& func)
    {
        _stdout = func;
        return *this;
    }

    /**
     * \brief Makes stdout append lines to the given vector
     */
    ScriptIo& output(std::vector<std::string>& lines)
    {
        _stdout = output_capture(lines);
        return *this;
    }

    /**
     * \brief Makes stdout use the engine logger
     */
    ScriptIo& output_log(
        const std::string& origin = "Python",
        logging::Verbosity verbosity = logging::Verbosity::Info
    )
    {
        _stdout = to_log(origin, verbosity);
        return *this;
    }

    /**
     * \brief Returns the error output functor
     */
    const Output& stderr() const
    {
        return _stderr;
    }

    /**
     * \brief Sets the error output functor
     */
    ScriptIo& error(const Output& func)
    {
        _stderr = func;
        return *this;
    }

    /**
     * \brief Makes stderr append lines to the given vector
     */
    ScriptIo& error(std::vector<std::string>& lines)
    {
        _stderr = output_capture(lines);
        return *this;
    }

    /**
     * \brief Makes stdout use the engine logger
     */
    ScriptIo& error_log(
        const std::string& origin = "Python",
        logging::Verbosity verbosity = logging::Verbosity::Info
    )
    {
        _stderr = to_log(origin, verbosity);
        return *this;
    }

    /**
     * \brief Redirects stderr to (the current) stdout
     */
    ScriptIo& error_to_out()
    {
        _stderr = _stdout;
        return *this;
    }

    /**
     * \brief Sets the exception handler
     */
    ScriptIo& set_exception_handler(const Exception& func)
    {
        _exception = func;
        return *this;
    }

    /**
     * \brief Invokes the exception handler
     */
    void exception(const boost::python::object& exception) const
    {
        return _exception(exception);
    }

private:
    static std::string noop_in() { return {}; }
    static void noop_out(const std::string& str) {}
    static void noop_exc(const boost::python::object& str) {}

    Input _stdin = &noop_in;
    Output _stdout = &noop_out;
    Output _stderr = &noop_out;
    Exception _exception = &noop_exc;

    Output output_capture(std::vector<std::string>& lines)
    {
        return [&lines](const std::string& line){ lines.push_back(line); };
    }

    Output to_log(const std::string& origin, logging::Verbosity verbosity)
    {
        return [origin, verbosity](const std::string& line){
            logging::Logger::instance().log(verbosity, origin, {line});
        };
    }
};

/**
 * \brief Python interpreter
 * \note Creating Interpreter objects is a cheap operation,
 *       the python scripting engine will be initialized only the first time
 */
class Interpreter
{
public:
    /**
     * \brief Ensures the interpreter is initialized
     */
    Interpreter();

    /**
     * \brief Executes some python code
     * \param python_code   Python sources to execute
     * \param io            I/O handler
     * \note Global variables are preserved between executions
     */
    void exec(const std::string& python_code, const ScriptIo& io);

    /**
     * \brief Executes some python code from a file
     * \param file          File containing python sources
     * \param io            I/O handler
     * \note Global variables are preserved between executions
     */
    void exec_file(const std::string& file, const ScriptIo& io);

    /**
     * \brief Global dict
     */
    boost::python::object globals() const
    {
        return _globals;
    }

    /**
     * \brief Sets a global variable
     */
    void set_global(const std::string& name, boost::python::object object)
    {
        _globals[boost::python::str(name.c_str())] = object;
    }

    /**
     * \brief Prepares the Interpreter for execution in a multi-threaded environment
     * \note This should be called in the main thread
     */
    static void enable_threading();

    /**
     * \brief Stops the execution of running scripts
     * \param message Message for the exception raised in python
     */
    static void stop(const std::string& message = "Script terminated from the engine");

private:
    /**
     * \brief Extracts exception information and sends it to \p io
     */
    void handle_exception(const ScriptIo& io, bool print_trace);

    /**
     * \brief Ensures the interpreter has been initialized
     */
    static void initialize();


    /**
     * \brief Initializes global data for execution
     * \returns \b true on success
     */
    bool patch_builtins(class ScriptIoProcessor& io);

    boost::python::dict _globals;
};

} // namespace scripting
#endif // MELANOENGINE_SCRIPTING_INTERPRETER_HPP
