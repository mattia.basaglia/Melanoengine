/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GRAPHICS_FONT_PAINT_HPP
#define MELANOENGINE_GRAPHICS_FONT_PAINT_HPP

#include "graphics/paint.hpp"
#include "graphics/font.hpp"

namespace graphics{

enum class TextVerticalAlign
{
    Top,        ///< The origin is on the top corner of the bounding box
    Baseline,   ///< The origin is on the baseline of the first line
    Middle,     ///< The origin is on the middle of the bounding box
    Bottom,     ///< The origin is on the bottom corner of the bounding box
};

/**
 * \brief Style on how to draw a piece of text
 */
struct FontPaint : public Paint
{
    FontPaint(
        Font font,
        Color color,
        BlendMode blend_mode = BlendMode::Alpha,
        TextVerticalAlign vertical_align = TextVerticalAlign::Top,
        float line_separation = 1.0
    ) : Paint(color, blend_mode),
        font(std::move(font)),
        vertical_align(vertical_align),
        line_separation(line_separation)
    {}

    Font font;
    TextVerticalAlign vertical_align;
    /// Distance between two lines as a factor of the recommended size
    float line_separation;
};

} // namespace graphics
#endif // MELANOENGINE_GRAPHICS_FONT_PAINT_HPP
