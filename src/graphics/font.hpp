/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GRAPHICS_FONT_HPP
#define MELANOENGINE_GRAPHICS_FONT_HPP

#include "backend/common/graphics/graphic_backend.hpp"
#include "backend/common/backend.hpp"
#include "graphics/font_style.hpp"

namespace graphics{


class Font
{
public:
    using Style = FontStyle;

    explicit Font(std::shared_ptr<backend::FontData> data)
        : data(std::move(data))
    {}

    Font(const std::string& filename, std::size_t point_size, Style style = Regular)
        : data(backend::Backend::instance().graphic().create_font(
            filename, point_size, style)
        )
    {}

    Style style() const
    {
        return data->style();
    }

    bool bold() const
    {
        return style() & Bold;
    }

    bool italic() const
    {
        return style() & Italic;
    }

    bool underline() const
    {
        return style() & Unerline;
    }

    bool regular() const
    {
        return style() == Regular;
    }

    /**
     * \brief Whether all glyphs have the same width
     */
    bool monospace() const
    {
        return data->monospace();
    }

    /**
     * \brief Maximum height of a glyph
     */
    Dimension max_height() const
    {
        return data->max_height();
    }

    /**
     * \brief Distance of the top of an ascender from the baseline
     */
    Dimension ascent() const
    {
        return data->ascent();
    }

    /**
     * \brief Distance of the top of an descender from the baseline
     */
    Dimension descent() const
    {
        return data->descent();
    }

    /**
     * \brief Recommended distance between two baselines
     */
    Dimension line_height() const
    {
        return data->line_height();
    }

    /**
     * \brief Font family name
     */
    std::string family() const
    {
        return data->family();
    }

    /**
     * \brief Whether the font can render the given character
     */
    bool has_character(const melanolib::string::Unicode& unicode) const
    {
        return data->has_character(unicode);
    }

    bool has_character(uint32_t unicode) const
    {
        return has_character(melanolib::string::Unicode(unicode));
    }

    bool has_character(char c) const
    {
        return has_character(melanolib::string::Unicode(std::string(1, c), c));
    }

    /**
     * \brief Describes graphical properties of a rendered glyph
     */
    GlyphMetrics glyph_metrics(const melanolib::string::Unicode& unicode) const
    {
        return data->glyph_metrics(unicode);
    }

    GlyphMetrics glyph_metrics(uint32_t unicode) const
    {
        return glyph_metrics(melanolib::string::Unicode(unicode));
    }

    GlyphMetrics glyph_metrics(char c) const
    {
        return glyph_metrics(melanolib::string::Unicode(std::string(1, c), c));
    }

    /**
     * \brief Size the given string will take when rendered
     * \note \p string must be encoded in utf-8
     */
    Size size(const std::string& string) const
    {
        return data->size(string);
    }

private:
    friend class Painter;
    std::shared_ptr<backend::FontData> data;
};

} // namespace graphics
#endif // MELANOENGINE_GRAPHICS_FONT_HPP
