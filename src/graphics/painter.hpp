/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GRAPHICS_PAINTER_HPP
#define MELANOENGINE_GRAPHICS_PAINTER_HPP

#include "graphics/window.hpp"
#include "graphics/image.hpp"
#include "graphics/font.hpp"
#include "graphics/paint.hpp"
#include "graphics/font_paint.hpp"

namespace graphics{

/**
 * \brief Performs drawing operations on a drawable surface
 */
class Painter
{
public:
    explicit Painter(Window& window)
        : data(backend::Backend::instance().graphic().create_painter(window.data.get()))
    {}

    explicit Painter(Image& image)
        : data(backend::Backend::instance().graphic().create_painter(image.data.get()))
    {}

    /**
     * \brief Ensures all paint operations appear on the target
     */
    void commit()
    {
        data->commit();
    }

    /**
     * \brief Fills the target with the given color
     */
    void fill(const Paint& style)
    {
        data->fill(style);
    }

    /**
     * \brief Fills a rectangle with a solid color
     */
    void draw_rect(const Paint& style, const Rectangle& rect)
    {
        data->draw_rect(style, rect);
    }

    void draw_line(const Paint& style, const Line& line)
    {
        data->draw_line(style, line);
    }

    void draw_pixel(const Paint& style, const Point& pos)
    {
        data->draw_pixel(style, pos);
    }

    /**
     * \brief Draws the whole image at the given location
     * \pre \p image has been created by the same backend as the target
     */
    void draw_image(const Image& image, const Point& pos)
    {
        draw_image(image, image.rect(), image.rect().translated(pos));
    }

    /**
     * \brief Draws a subset of the image at the given location
     * \pre \p image has been created by the same backend as the target
     */
    void draw_image(const Image& image, const Rectangle& source, const Point& pos)
    {
        draw_image(image, source, Rectangle(pos, source.size()));
    }

    /**
     * \brief Draws a subset of the image stretching it to fill the destination
     * \pre \p image has been created by the same backend as the target
     */
    void draw_image(const Image& image, const Rectangle& source, const Rectangle& dest)
    {
        data->draw_image(image.data.get(), source, dest, false, false);
    }

    /**
     * \brief Draws a subset of the image at the given location
     * \pre \p image has been created by the same backend as the target
     */
    void draw_image(const SubImage& image, const Point& pos)
    {
        draw_image(image, Rectangle(pos, image.size()));
    }

    /**
     * \brief Draws a subset of the image stretching it to fill the destination
     * \pre \p image has been created by the same backend as the target
     */
    void draw_image(const SubImage& image, const Rectangle& dest)
    {
        data->draw_image(
            image.image().data.get(),
            image.rect(),
            dest,
            image.horizontal_flip(),
            image.vertical_flip()
        );
    }

    /**
     * \brief Draws the given string in the selected style
     * \param style     Text style
     * \param text      Utf-8 encoded string
     * \param origin    Its semantics are defined by \p style.vertical_align
     * \returns The bounding box of the rendered text
     * \pre \p style.font has been created by the same back-end as the target
     */
    Rectangle draw_text(const FontPaint& style, const std::string& text, const Point& origin)
    {
        return data->draw_text(style, style.font.data.get(), text, origin);
    }

private:
    std::unique_ptr<backend::PainterData> data;
};

} // namespace graphics
#endif // MELANOENGINE_GRAPHICS_PAINTER_HPP
