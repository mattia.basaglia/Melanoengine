/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GRAPHICS_WINDOW_HPP
#define MELANOENGINE_GRAPHICS_WINDOW_HPP

#include "backend/common/graphics/graphic_backend.hpp"
#include "backend/common/backend.hpp"
#include "graphics/geometry.hpp"

namespace graphics {

/**
 * \brief A window on the desktop environment
 */
class Window
{
public:
    /**
     * \brief Creates a window
     */
    Window(
        Rectangle rect,
        const std::string& title = "",
        bool fullscreen = false
    ) : data(backend::Backend::instance().graphic().create_window(rect, title, fullscreen))
    {}

    /**
     * \brief Creates a centered window
     */
    Window(
        Size size,
        const std::string& title = "",
        bool fullscreen = false
    ) : data(backend::Backend::instance().graphic().create_window(size, title, fullscreen))
    {}

    Size size() const
    {
        return data->size();
    }

    Point pos() const
    {
        return data->pos();
    }

    Rectangle rect() const
    {
        return data->rect();
    }

    void move(Point pos)
    {
        return data->move(pos);
    }

    void move_to_center()
    {
        return data->move_to_center();
    }

    void resize(Size size)
    {
        return data->resize(size);
    }

    bool fullscreen() const
    {
        return data->fullscreen();
    }

    void set_fullscreen(bool fullscreen)
    {
        return data->set_fullscreen(fullscreen);
    }

    std::string title() const
    {
        return data->title();
    }

    void set_title(const std::string& title)
    {
        return data->set_title(title);
    }

private:
    friend class Painter;
    friend class Image;
    std::unique_ptr<backend::WindowData> data;
};

} // namespace graphics
#endif // MELANOENGINE_GRAPHICS_WINDOW_HPP
