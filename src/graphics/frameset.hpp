/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GRAPHICS_FRAMESET_HPP
#define MELANOENGINE_GRAPHICS_FRAMESET_HPP

#include <unordered_map>

#include "graphics/image.hpp"

namespace graphics{

/**
 * \brief A collection of sub-images (which may or may not share the same image)
 */
class FrameSet : public std::enable_shared_from_this<FrameSet>
{
public:
    using size_type = int;

    virtual ~FrameSet(){}

    /**
     * \brief Retrieves the image at the given location
     */
    virtual SubImage image_at(size_type index) const = 0;

    /**
     * \brief Number of images
     */
    virtual size_type size() const = 0;

    /**
     * \brief Size of a single frame
     */
    virtual Size frame_size() const = 0;
};

/**
 * \brief FrameSet from a group of images
 */
class FrameCollection : public FrameSet
{
public:
    SubImage image_at(size_type index) const override
    {
        auto iter = images.find(index);
        if ( iter == images.end() )
            return SubImage();
        return iter->second.sub_image(max_rect);
    }

    /**
     * \brief Sets an image at a specified index
     */
    void set_image(size_type index, const Image& image)
    {
        auto it = images.find(index);
        if ( it != images.end() )
            it->second = image;
        else
            images.insert({index, image});
        
        if ( images.size() == 1 )
            max_rect = image.rect();
        else
            max_rect |= image.rect();
    }

    /**
     * \brief Add an image generating an index automatically
     * \note To have consistent effects all images must have sequential
     * (zero-based) indices.
     */
    void append(const Image& image)
    {
        set_image(images.size(), image);
    }

    size_type size() const override
    {
        return images.size();
    }

    /**
     * \note This is determined based on the greatest image.
     * For best results only insert images with the same size.
     */
    Size frame_size() const override
    {
        return max_rect.size();
    }

private:
    std::unordered_map<size_type, Image> images;
    Rectangle max_rect;
};

/**
 * \brief Frame set created from a grid subdivision of a single image
 */
class SpriteSheet : public FrameSet
{
public:
    /**
     * \param image         (Sub)Image to consider for the frames
     * \param count_x       Number of frames on the X axis
     * \param count_y       Number of frames on the Y axis
     */
    SpriteSheet(SubImage image, Dimension count_x, Dimension count_y)
        : _image(std::move(image)),
          _frame_size(_image.width() / count_x, _image.height() / count_y),
          _gap(0)
    {}

    /**
     * \param image         (Sub)Image to consider for the frames
     * \param frame_size    Size of a single frame
     * \param gap           Gap between frames in pixels
     */
    SpriteSheet(SubImage image, Size frame_size, Dimension gap = 0)
        : _image(std::move(image)), _frame_size(frame_size), _gap(gap)
    {}

    /**
     * \param image         Image to consider for the frames
     * \param frame_size    Size of a single frame
     * \param gap           Gap between frames in pixels
     */
    SpriteSheet(Image& image, Size frame_size, Dimension gap = 0)
        : SpriteSheet(image.sub_image(image.rect()), frame_size, gap)
    {}

    SubImage image_at(size_type index) const override
    {
        if ( !_image )
            return SubImage();

        auto ntiles_x = tiles_x();
        if ( ntiles_x == 0 )
            return SubImage();

        return image_at_grid(index % ntiles_x, index / ntiles_x);
    }

    /**
     * \brief Returns the image at the given grid coordinates
     */
    SubImage image_at_grid(size_type index_x, size_type index_y) const
    {
        if ( !_image )
            return SubImage();

        auto area = _image.rect();
        Point pos {
            index_x * (_frame_size.width + _gap),
            index_y * (_frame_size.height + _gap),
        };

        // Going over => force invalid tile
        if ( pos.y > _image.size().height )
            return SubImage();

        return _image.image().sub_image(Rectangle{
            area.top_left() + pos,
            _frame_size
        });
    }

    /**
     * \brief Returns the linear indef for the given grid coordinates
     */
    size_type index_at_grid(size_type index_x, size_type index_y) const
    {
        return index_x + index_y * tiles_x();
    }

    size_type size() const override
    {
        return tiles_x() * tiles_y();
    }

    Size frame_size() const override
    {
        return _frame_size;
    }

private:
    size_type tiles_x() const
    {
        return (_image.size().width + _gap) / (_frame_size.width + _gap);
    }

    size_type tiles_y() const
    {
        return (_image.size().height + _gap) / (_frame_size.height + _gap);
    }

    SubImage _image;
    Size _frame_size;
    Dimension _gap;
};

} // namespace graphics
#endif // MELANOENGINE_GRAPHICS_FRAMESET_HPP
