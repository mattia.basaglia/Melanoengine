/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GRAPHICS_ANIMATION_HPP
#define MELANOENGINE_GRAPHICS_ANIMATION_HPP

#include <type_traits>
#include <chrono>

#include "melanolib/math/random.hpp"

#include "frameset.hpp"

namespace graphics{

using Duration = std::chrono::milliseconds;

class Animation : public std::enable_shared_from_this<Animation>
{
public:
    using AnimationFlags = uint16_t;

    enum AnimationFlagsEnum : AnimationFlags
    {
        Default     = 0x00,
        NoLoop      = 0x01, ///< Doesn't loop around once the last frame is reached
        Randomized  = 0x02, ///< The first frame is random
        UserDefined = 0x10, ///< User-defined flags must be maskable bit patterns
                            ///  greater than or equal to this
    };

    using FrameIndex = Dimension;

    /**
     * \param frameset      Frameset to get the images from
     * \param start_frame   First frame index in \p frameset
     * \param n_frames      Number of (consecutive) frame indices in \p frameset
     * \param fps           Animation speed in frames per seconds
     *                      If 0, it should act as a static image
     * \param flags         Animation flags
     * \param offset        Offset of frames from an object's origin
     */
    Animation(
        std::shared_ptr<FrameSet> frameset,
        FrameIndex start_frame,
        FrameIndex n_frames,
        Dimension fps,
        AnimationFlags flags = Default,
        const Point& offset = {}
    ) : _frameset(std::move(frameset)),
        _start_frame(start_frame),
        _n_frames(n_frames),
        _fps(fps),
        _flags(flags),
        _offset(offset)
    {
        if ( ! _frameset )
        {
            throw engine::Error("Creating an animation without a frameset", "Graphics");
        }
    }

    Animation(
        FrameSet& frameset,
        FrameIndex start_frame,
        FrameIndex n_frames,
        Dimension fps,
        AnimationFlags flags = Default,
        const Point& offset = {}
    ) : Animation(frameset.shared_from_this(), start_frame, n_frames,
                  fps, flags, offset)
    {}

    /**
     * \brief Get the frame image at the given index
     * \pre \p index is a correct frame index (ie: returned by next_frame())
     */
    SubImage frame(FrameIndex index) const
    {
        return _frameset->image_at(_start_frame + index);
    }

    /**
     * \brief Get a relative frame in the animation
     * \param previous  The starting frame
     * \param count     Number of steps to move forward by.
     *                  If negative it moves backwards.
     */
    FrameIndex next_frame(FrameIndex previous, int count = 1) const
    {
        if ( !_n_frames )
            return 0;

        FrameIndex next = (previous + count) % _n_frames;
        if ( next < 0 )
            next += _n_frames;

        if ( (_flags & Randomized) && next == 0 )
            return melanolib::math::random(_n_frames - 1);

        if ( _fps == 0 )
            return previous;

        if ( _flags & NoLoop )
        {
            if ( previous + count >= _n_frames )
                return _n_frames - 1;

            if ( previous + count < 0 )
                return 0;
        }

        return next;
    }

    /**
     * \brief Animation speed in frames per second
     */
    Dimension fps() const
    {
        return _fps;
    }

    /**
     * \brief Get the time a frame shall last for
     * \note Returns a zero time if the animation has zero speed
     */
    Duration frame_time() const
    {
        if ( !_fps )
            return Duration::zero();
        return Duration(Duration::period::den / _fps);
    }

    /**
     * \brief Checks whether a frame is the last of the animation
     */
    bool is_last_frame(FrameIndex frame) const
    {
        return (_flags & NoLoop) && frame >= _n_frames - 1;
    }

    /**
     * \brief The offset this animation should be rendered from the object origin
     */
    Point offset() const
    {
        return _offset;
    }

    /**
     * \brief Changes the offset
     * \see offset()
     */
    void set_offset(const Point& offset)
    {
        _offset = offset;
    }

private:
    std::shared_ptr<FrameSet> _frameset;
    FrameIndex _start_frame;
    FrameIndex _n_frames;
    Dimension _fps;
    AnimationFlags _flags;
    Point _offset;
};


} // namespace graphics
#endif // MELANOENGINE_GRAPHICS_ANIMATION_HPP
