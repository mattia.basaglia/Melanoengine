/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GRAPHICS_PAINT_HPP
#define MELANOENGINE_GRAPHICS_PAINT_HPP

#include "melanolib/color/color.hpp"

namespace graphics{

/**
 * \brief RGBA Color
 */
using melanolib::color::Color;

/**
 * \brief Expresses how transparency should be handled
 */
enum class BlendMode
{
    /// No trasparency:
    /// dest.rgb = src.rgb
    NoBlend,
    /// Alpha blending (linear interpolation):
    /// dest.rgb = (src.rgb * src.a) + (dest.rgb * (1 - src.a))
    /// dest.a = src.a + dest.a * (1 - src.a)
    Alpha,
    /// Additive blending
    /// dest.rgb = src.rgb * src.a) + dest.rgb
    Add,
};

/**
 * \brief Paint used for drawing
 */
struct Paint
{
    Paint(Color color, BlendMode blend_mode = BlendMode::Alpha)
        : color(color), blend_mode(blend_mode)
    {}

    Color color;
    BlendMode blend_mode;
};

class FontPaint;

} // namespace graphics
#endif // MELANOENGINE_GRAPHICS_PAINT_HPP
