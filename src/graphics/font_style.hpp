/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GRAPHICS_FONT_STYLE_HPP
#define MELANOENGINE_GRAPHICS_FONT_STYLE_HPP

#include "graphics/geometry.hpp"

namespace graphics{

using FontStyle = unsigned;

enum FontStyleEnum : FontStyle
{
    Regular     = 0x0,
    Bold        = 0x1,
    Italic      = 0x2,
    Unerline    = 0x4,
};

/**
 * \brief Structure defining rendering metrics for a glyph
 */
struct GlyphMetrics
{
    /// Bounding box of the redered portion of the glyph,
    /// relative to its origin on the baseline
    Rectangle box;
    /// Amount to move from the origin of the glyph to reach the next
    Point advance;
};

} // namespace graphics
#endif // MELANOENGINE_GRAPHICS_FONT_STYLE_HPP
