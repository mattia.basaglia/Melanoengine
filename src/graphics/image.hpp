/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GRAPHICS_IMAGE_HPP
#define MELANOENGINE_GRAPHICS_IMAGE_HPP

#include "graphics/window.hpp"
#include "melanolib/utils/c++-compat.hpp"

namespace graphics{

class Image
{
    friend class Painter;

public:
    explicit Image(std::shared_ptr<backend::ImageData> data)
        : data(std::move(data))
    {}

    /**
     * \brief Opens the given file as an image
     */
    explicit Image(const std::string& filename)
        : data(backend::Backend::instance().graphic().create_image(filename))
    {}

    /**
     * \brief Creates a new image
     */
    explicit Image(const Size& size, const Color& color = {255, 255, 255, 0})
        : data(backend::Backend::instance().graphic().create_image(size, color))
    {}

    /**
     * \brief Creates a screenshot of the given window
     */
    explicit Image(const Window& window)
        : data(backend::Backend::instance().graphic().create_image(window.data.get()))
    {}

    Dimension width() const
    {
        return data->size().width;
    }

    Dimension height() const
    {
        return data->size().height;
    }

    Size size() const
    {
        return data->size();
    }

    Rectangle rect() const
    {
        return Rectangle(Point(0, 0), data->size());
    }

    /**
     * \brief Creates a subset of this image
     * \note Can only be used if \b this is managed by a shared pointer
     */
    class SubImage sub_image(const Rectangle& rect) const;

    /**
     * \brief Creates a flipped (sub)image
     * \note Can only be used if \b this is managed by a shared pointer
     */
    class SubImage flipped(bool horizontal=true, bool vertical=false) const;

    /**
     * \brief Saves the image with the given filename
     */
    void save(const std::string& filename) const
    {
        return data->save(filename);
    }

    /**
     * \brief Checks if two images point to the same surface data
     */
    bool operator==(const Image& other) const
    {
        return data == other.data;
    }

    bool operator!=(const Image& other) const
    {
        return data != other.data;
    }

private:
    std::shared_ptr<backend::ImageData> data;
};

/**
 * \brief A subsection of an image
 */
class SubImage
{
public:
    SubImage(Image image)
        : _image(std::move(image)),
          _rect(_image->rect()),
          _hflipped(false),
          _vflipped(false)
    {}

    SubImage(Image image, const Rectangle& rect,
             bool horizontal_flip = false, bool vertical_flip = false)
        : _image(std::move(image)),
          _rect(rect),
          _hflipped(horizontal_flip),
          _vflipped(vertical_flip)
    {}

    /**
     * \brief Builds an invalid sub-image
     */
    SubImage(){}

    Size size() const
    {
        return _rect.size();
    }

    Dimension width() const
    {
        return _rect.width;
    }

    Dimension height() const
    {
        return _rect.height;
    }

    Rectangle rect() const
    {
        return _rect;
    }

    void set_rect(const Rectangle& rect)
    {
        _rect = rect;
    }

    /**
     * \brief Returns a reference to the underlying image
     * \pre valid()
     */
    const Image& image() const
    {
        return *_image;
    }

    bool valid() const
    {
        return bool(_image);
    }

    explicit operator bool() const
    {
        return valid();
    }

    /**
     * \brief Flips the image
     */
    void flip(bool horizontal=true, bool vertical=false)
    {
        if ( horizontal )
            _hflipped = !_hflipped;
        if ( vertical )
            _vflipped = !_vflipped;
    }

    /**
     * \brief Set a specific flip relative to the source image
     */
    void set_flip(bool horizontal, bool vertical)
    {
        _hflipped = horizontal;
        _vflipped = vertical;
    }

    void set_horizontal_flip(bool flip)
    {
        _hflipped = flip;
    }

    void set_vertical_flip(bool flip)
    {
        _vflipped = flip;
    }

    bool horizontal_flip() const
    {
        return _hflipped;
    }

    bool vertical_flip() const
    {
        return _vflipped;
    }

    /**
     * \brief Returns the flipped image
     */
    SubImage flipped(bool horizontal=true, bool vertical=false)
    {
        SubImage copy = *this;
        copy.flip(horizontal, vertical);
        return copy;
    }

private:
    melanolib::Optional<Image> _image;
    Rectangle _rect;
    bool _hflipped = false;
    bool _vflipped = false;
};

inline SubImage Image::sub_image(const Rectangle& rect) const
{
    return SubImage(*this, rect);
}

inline SubImage Image::flipped(bool horizontal, bool vertical) const
{
    return SubImage(*this, rect(), horizontal, vertical);
}

} // namespace graphics
#endif // MELANOENGINE_GRAPHICS_IMAGE_HPP
