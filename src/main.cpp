/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <boost/program_options.hpp>

#include "melanolib/string/simple_stringutils.hpp"

#include "engine/engine.hpp"
#include "engine/version.hpp"
#include "engine/data.hpp"

#include "backend/common/types.hpp"
#include "backend/common/backend.hpp"

boost::program_options::options_description options()
{
    using namespace boost::program_options;
    options_description desc("Options");

    options_description desc_info("Information");
    desc_info.add_options()
        ("help,h", "Shows this message and exits")
        ("version,V", "Shows version information and exits")
        ("license,l", "Shows license information and exits")
    ;
    desc.add(desc_info);

    options_description desc_general("General Options");
    desc_general.add_options()
        (
            "verbosity,v",
            value<std::string>()
                ->value_name("level")
                ->implicit_value(
                    logging::Logger::instance().verbosity_to_string(
                    logging::Verbosity::Debug
                ))
                ->default_value(logging::Logger::instance().verbosity_to_string(
                    logging::Logger::instance().verbosity()
                )),
            ("Changes log verbosity level\n"
            "Possible values: " +
            melanolib::string::implode(", ", logging::Logger::instance().verbosity_names())
            ).c_str()

        )
        (
            "include,I",
            value<std::vector<std::string>>()->value_name("path"),
            "Adds a data search path"
        )
        (
            "output,o",
            value<std::vector<std::string>>()->value_name("path"),
            "Adds a data output path"
        )
        (
            "exec",
            value<std::vector<std::string>>()->value_name("path"),
            "Path to scripts files in the data directory to run at start-up"
        )
        (
            "watchdog",
            value<std::size_t>()->value_name("seconds")->default_value(2),
            "Delay between watchdog checks"
        )
    ;
    desc.add(desc_general);

    options_description desc_backend("Back-end Options");
    desc_backend.add_options()
        (
            "graphics,g",
            value<std::string>()
                ->value_name("backend")
                ->default_value(backend::GraphicBackend::factory().default_name()),
            ("Selects the graphics back-end.\n"
            "Possible values: " +
            melanolib::string::implode(", ", backend::GraphicBackend::factory().available())
            ).c_str()
        )
        (
            "input,i",
            value<std::string>()
                ->value_name("backend")
                ->default_value(backend::InputBackend::factory().default_name()),
            ("Selects the input back-end.\n"
            "Possible values: " +
            melanolib::string::implode(", ", backend::InputBackend::factory().available())
            ).c_str()

        )
    ;
    desc.add(desc_backend);

    return desc;
}

boost::program_options::variables_map parse(
    const boost::program_options::options_description& desc,
    int argc,
    char** argv
)
{
    using namespace boost::program_options;
    auto parsed = parse_command_line(argc, argv, desc);
    variables_map options;
    store(parsed, options);
    notify(options);
    return options;
}

int main(int argc, char** argv)
{
    backend::Backend::load_factories();

    auto option_description = options();
    auto options = parse(option_description, argc, argv);

    if ( options.count("help") )
    {
        std::cout << option_description << "\n";
        return 0;
    }

    if ( options.count("version") )
    {
        std::cout << engine::version::name << ' '
                  << engine::version::version << ' '
                  << '(' << engine::version::version_verbose << ')'
                  << "\n";
        return 0;
    }

    if ( options.count("license") )
    {
        std::cout << engine::version::license_text << "\n";
        return 0;
    }

    try
    {
        logging::Logger::instance().set_verbosity(
            logging::Logger::instance().verbosity_from_string(
                options["verbosity"].as<std::string>()
            )
        );

        engine::Data::instance().load_paths(argv[0]);
        if ( options.count("include") )
        {
            for ( const auto& path: options["include"].as<std::vector<std::string>>() )
                engine::Data::instance().add_path(path);
        }

        if ( options.count("output") )
        {
            for ( const auto& path: options["output"].as<std::vector<std::string>>() )
                engine::Data::instance().add_writable_path(path);
        }

        backend::Backend::instance().initialize(
            options["graphics"].as<std::string>(),
            options["input"].as<std::string>()
        );

        engine::Engine& engine = engine::Engine::instance();

        if ( options.count("exec") )
        {
            for ( const auto& script: options["exec"].as<std::vector<std::string>>() )
                engine.async(&engine::Engine::run_script_file, script);
        }

        if ( options.count("watchdog") )
            engine.set_watchdog_delay(std::chrono::seconds(
                options["watchdog"].as<std::size_t>())
            );

        engine.run();

        return 0;
    }
    catch(const engine::Error& error)
    {
        logging::critical(error.origin()) << error.what();
    }
    catch(const std::exception& error)
    {
        logging::critical() << error.what();
    }

    return 1;
}
