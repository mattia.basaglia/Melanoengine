/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#ifndef MELANOENGINE_GAME_ASSET_MANAGER_HPP
#define MELANOENGINE_GAME_ASSET_MANAGER_HPP

#include <memory>
#include <string>

#include <boost/core/noncopyable.hpp>

#include "melanolib/data_structures/cache.hpp"
#include "melanolib/data_structures/hash.hpp"

#include "engine/error.hpp"
#include "graphics/font_style.hpp"

namespace graphics {
class Image;
class FrameSet;
class Animation;
class Font;
} // namespace graphics

namespace backend {

class ImageData;
class FontData;

} // namespace backend

namespace game{

class AssetManager : public boost::noncopyable
{
public:
    template<class Element>
    class AssetRegistry
    {
    public:
        using pointer = std::shared_ptr<Element>;

        /**
         * \brief Adds (or replaces) an item
         */
        void add(const std::string& name, const pointer& value)
        {
            if ( !value )
                remove(name);
            else
                _elements[name] = value;
        }

        /**
         * \brief Removes an item
         */
        void remove(const std::string& name)
        {
            _elements.erase(name);
        }

        /**
         * \brief Removes all items
         */
        void clear()
        {
            _elements.clear();
        }

        /**
         * \brief Returns a pointer to the selected element
         * \throws engine::Error if \p name is not found
         */
        pointer get(const std::string& name) const
        {
            auto iter = _elements.find(name);
            if ( iter == _elements.end() )
                throw engine::Error("Item not found:" + name, "Media");
            return iter->second;
        }

        /**
         * \brief Returns whether the registry contains the given item
         */
        bool contains(const std::string& name) const
        {
            return _elements.count(name);
        }

        /**
         * \brief Returns the number of items
         */
        std::size_t size() const
        {
            return _elements.size();
        }

    private:
        friend AssetManager;
        AssetRegistry(){}

        std::unordered_map<std::string, pointer> _elements;
    };

    /**
     * \brief Retrieves an image
     * \param name Data image name
     *
     * Loads the image from the appropriate data directory, caches
     * recently accessed images to reduce I/O operations
     */
    graphics::Image image(const std::string& name);

    /**
     * \brief Maximum number of images to keep in the cache
     */
    std::size_t images_max() const
    {
        return _images.max_size();
    }

    /**
     * \brief Sets the maximum number of images to keep in the cache
     */
    void set_images_max(std::size_t size)
    {
        _images.set_max_size(size);
    }

    /**
     * \brief Number of images currently in the cache
     */
    std::size_t images() const
    {
        return _images.size();
    }

    /**
     * \brief Number of images currently in the cache or with lingering
     * references outside the cache
     */
    std::size_t images_all() const
    {
        return _images.size_shared();
    }

    /**
     * \brief Retrieves a font
     * \param name Data font name
     *
     * Loads the font from the appropriate data directory, caches
     * recently accessed images to reduce I/O operations
     */
    graphics::Font font(
        const std::string& name,
        graphics::Dimension point_size,
        graphics::FontStyle style = graphics::Regular
    );

    /**
     * \brief Maximum number of fonts to keep in the cache
     */
    std::size_t fonts_max() const
    {
        return _fonts.max_size();
    }

    /**
     * \brief Sets the maximum number of fonts to keep in the cache
     */
    void set_fonts_max(std::size_t size)
    {
        _fonts.set_max_size(size);
    }

    /**
     * \brief Number of fonts currently in the cache
     */
    std::size_t fonts() const
    {
        return _fonts.size();
    }

    /**
     * \brief Number of fonts currently in the cache or with lingering
     * references outside the cache
     */
    std::size_t fonts_all() const
    {
        return _fonts.size_shared();
    }

    /**
     * \brief Crear stored data
     * \param flush_cache If \b true, remove cached assets
     */
    void clear(bool flush_cache = false)
    {
        _animations.clear();
        _frame_sets.clear();

        if ( flush_cache )
        {
            _fonts.clear();
            _images.clear();
        }
    }

    /**
     * \brief Returns the frameset manager
     */
    auto& framesets()
    {
        return _frame_sets;
    }

    /**
     * \brief Returns the animation manager
     */
    auto& animations()
    {
        return _animations;
    }

private:
    melanolib::SharedObjectLruCache<std::string, backend::ImageData> _images{32};
    using FontKey = std::tuple<std::string, graphics::Dimension, graphics::FontStyle>;
    melanolib::SharedObjectLruCache<FontKey, backend::FontData, melanolib::TupleHasher> _fonts{8};
    AssetRegistry<graphics::FrameSet> _frame_sets;
    AssetRegistry<graphics::Animation> _animations;
};

} // namespace game
#endif // MELANOENGINE_GAME_ASSET_MANAGER_HPP
