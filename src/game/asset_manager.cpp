/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "asset_manager.hpp"

#include "engine/data.hpp"
#include "graphics/image.hpp"
#include "graphics/font.hpp"

namespace game{

graphics::Image AssetManager::image(const std::string& name)
{
    if ( _images.contains(name) )
        return graphics::Image(_images.at(name));

    auto ptr = backend::Backend::instance().graphic().create_image(
        engine::Data::instance().require_readable_path(name)
    );
    _images.insert(name, ptr);
    return graphics::Image(ptr);
}

graphics::Font AssetManager::font(
    const std::string& name,
    graphics::Dimension point_size,
    graphics::FontStyle style
)
{
    FontKey key{name, point_size, style};
    if ( _fonts.contains(key) )
        return graphics::Font(_fonts.at(key));

    auto ptr = backend::Backend::instance().graphic().create_font(
        engine::Data::instance().require_readable_path(name),
        point_size,
        style
    );
    _fonts.insert(key, ptr);
    return graphics::Font(ptr);
}

} // namespace game
