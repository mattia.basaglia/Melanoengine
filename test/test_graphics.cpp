/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "test.hpp"

#include "backend/dummy/graphics.hpp"
#include "backend/common/backend.hpp"
#include "graphics/window.hpp"
#include "graphics/animation.hpp"

using namespace graphics;

bool init_unit_test()
{
    backend::dummy::DummyGraphics::register_dummy();
    backend::Backend::instance().initialize("dummy");
    return true;
}

int main(int argc, char* argv[])
{
    return boost::unit_test::unit_test_main( &init_unit_test, argc, argv );
}


BOOST_AUTO_TEST_CASE( test_window_frontend )
{
    Window window(Rectangle(1, 2, 3, 4), "hello", false);

    BOOST_TEST(!window.fullscreen());
    window.set_fullscreen(true);
    BOOST_TEST(window.fullscreen());

    BOOST_CHECK(window.rect() == Rectangle(1, 2, 3, 4));
    BOOST_CHECK(window.pos() == Point(1, 2));
    window.move(Point(2, 1));
    BOOST_CHECK(window.pos() == Point(2, 1));
    BOOST_CHECK(window.rect() == Rectangle(2, 1, 3, 4));

    BOOST_CHECK(window.size() == Size(3, 4));
    window.resize(Size(4, 3));
    BOOST_CHECK(window.size() == Size(4, 3));
    BOOST_CHECK(window.rect() == Rectangle(2, 1, 4, 3));

    window.move_to_center();
    BOOST_CHECK(window.pos() == Point(0, 0));

    BOOST_CHECK(window.title() == "hello");
    window.set_title("world");
    BOOST_CHECK(window.title() == "world");
}

BOOST_AUTO_TEST_CASE( test_sub_image_from_image )
{
    Image image(Size(100, 100));
    SubImage sub = image.sub_image(Rectangle(10, 20, 30, 40));
    BOOST_TEST(sub.width() == 30);
    BOOST_TEST(sub.height() == 40);
    BOOST_TEST(sub.size().width == 30);
    BOOST_TEST(sub.size().height == 40);
    BOOST_CHECK(sub.rect() == Rectangle(10, 20, 30, 40));
    BOOST_TEST(sub.image() == image);
    BOOST_TEST(sub.image() == image);
    BOOST_TEST(!sub.horizontal_flip());
    BOOST_TEST(!sub.vertical_flip());
}

BOOST_AUTO_TEST_CASE( test_sub_image_flips )
{
    Image image(Size(100, 100));
    SubImage sub(image, Rectangle(10, 20, 30, 40));
    BOOST_TEST(!sub.horizontal_flip());
    BOOST_TEST(!sub.vertical_flip());
    sub.flip();
    BOOST_TEST(sub.horizontal_flip());
    BOOST_TEST(!sub.vertical_flip());
    sub.flip();
    BOOST_TEST(!sub.horizontal_flip());
    BOOST_TEST(!sub.vertical_flip());
    sub.flip(false, true);
    BOOST_TEST(!sub.horizontal_flip());
    BOOST_TEST(sub.vertical_flip());
    sub.set_flip(true, true);
    BOOST_TEST(sub.horizontal_flip());
    BOOST_TEST(sub.vertical_flip());
    sub.set_flip(true, false);
    BOOST_TEST(sub.horizontal_flip());
    BOOST_TEST(!sub.vertical_flip());
    sub.set_flip(false, false);
    BOOST_TEST(!sub.horizontal_flip());
    BOOST_TEST(!sub.vertical_flip());
    sub.set_horizontal_flip(true);
    BOOST_TEST(sub.horizontal_flip());
    BOOST_TEST(!sub.vertical_flip());
    sub.set_horizontal_flip(false);
    sub.set_vertical_flip(true);
    BOOST_TEST(!sub.horizontal_flip());
    BOOST_TEST(sub.vertical_flip());
}

BOOST_AUTO_TEST_CASE( test_frame_collection )
{
    std::vector<Image> images;
    images.push_back(Image(Size(10, 10)));
    images.push_back(Image(Size(10, 5)));
    images.push_back(Image(Size(30, 7)));

    FrameCollection frame_set;
    BOOST_TEST(frame_set.size() == 0);
    BOOST_CHECK(frame_set.frame_size() == Size());
    BOOST_TEST(!frame_set.image_at(0).valid());

    frame_set.append(images[0]);
    BOOST_TEST(frame_set.size() == 1);
    BOOST_CHECK(frame_set.frame_size() == Size(10, 10));
    BOOST_TEST(frame_set.image_at(0).image() == images[0]);
    BOOST_TEST(!frame_set.image_at(1).valid());

    frame_set.append(images[1]);
    BOOST_TEST(frame_set.size() == 2);
    BOOST_CHECK(frame_set.frame_size() == Size(10, 10));
    BOOST_TEST(frame_set.image_at(0).image() == images[0]);
    BOOST_TEST(frame_set.image_at(1).image() == images[1]);
    BOOST_CHECK(frame_set.image_at(1).size() == frame_set.frame_size());

    frame_set.set_image(123, images[2]);
    BOOST_TEST(frame_set.size() == 3);
    BOOST_CHECK(frame_set.frame_size() == Size(30, 10));
    BOOST_TEST(frame_set.image_at(0).image() == images[0]);
    BOOST_TEST(frame_set.image_at(1).image() == images[1]);
    BOOST_TEST(frame_set.image_at(123).image() == images[2]);

    frame_set.set_image(1, images[2]);
    BOOST_TEST(frame_set.image_at(1).image() == images[2]);
}

BOOST_AUTO_TEST_CASE( test_sprite_sheet )
{
    int padding = 20, spacing = 10, size = 30, count_x = 7, count_y = 4;
    int width = padding * 2 + size * count_x + spacing * (count_x-1);
    int height = padding * 2 + size * count_y + spacing * (count_y-1);

    Image image(Size(width, height));

    SpriteSheet sheet(
        image.sub_image(Rectangle(padding, padding,
                                   width - padding * 2, height - padding * 2)),
        Size(size, size),
        spacing
    );

    BOOST_TEST(sheet.size() == count_x * count_y);
    BOOST_CHECK(
        sheet.image_at(0).rect() ==
        Rectangle(padding, padding, size, size)
    );
    BOOST_CHECK(
        sheet.image_at(1).rect() ==
        Rectangle(padding + size + spacing, padding, size, size)
    );
    BOOST_CHECK(
        sheet.image_at(2).rect() ==
        Rectangle(padding + (size + spacing)*2, padding, size, size)
    );
    BOOST_CHECK(
        sheet.image_at(6).rect() ==
        Rectangle(padding + (size + spacing)*6, padding, size, size)
    );
    BOOST_CHECK(
        sheet.image_at(7).rect() ==
        Rectangle(padding, padding + size + spacing, size, size)
    );
    BOOST_CHECK(sheet.image_at(9).rect() == sheet.image_at_grid(2, 1).rect());
    BOOST_TEST(sheet.index_at_grid(2, 1) == 9);
    BOOST_CHECK(
        sheet.image_at(9).rect() ==
        Rectangle(padding + (size + spacing)*2, padding + size + spacing, size, size)
    );
    BOOST_CHECK(
        sheet.image_at(16).rect() ==
        Rectangle(padding + (size + spacing)*2, padding + (size + spacing)*2, size, size)
    );
    BOOST_CHECK(
        sheet.image_at(27).rect() ==
        Rectangle(padding + (size + spacing)*6, padding + (size + spacing)*3, size, size)
    );
    BOOST_CHECK(!sheet.image_at(28).valid());
}

BOOST_AUTO_TEST_CASE( test_sprite_sheet_auto_size )
{
    int size = 30, count_x = 7, count_y = 4;

    Image image(Size(count_x*size, count_y*size));

    SpriteSheet sheet(image, count_x, count_y);


    BOOST_TEST(sheet.size() == count_x * count_y);
    BOOST_CHECK(
        sheet.image_at(9).rect() ==
        Rectangle(size * 2, size, size, size)
    );
}

BOOST_AUTO_TEST_CASE( test_animation_looping )
{
    int size = 30, count_x = 7, count_y = 4;
    Image image(Size(count_x*size, count_y*size));
    auto frameset = std::make_shared<SpriteSheet>(image, count_x, count_y);

    Animation animation(frameset, frameset->index_at_grid(0, 2), 8, 24);
    BOOST_CHECK(animation.frame(0).rect() == frameset->image_at_grid(0, 2).rect());
    BOOST_CHECK(animation.frame(1).rect() == frameset->image_at_grid(1, 2).rect());
    BOOST_CHECK(animation.frame(7).rect() == frameset->image_at_grid(0, 3).rect());

    BOOST_TEST(animation.next_frame(0, 0) == 0);
    BOOST_TEST(animation.next_frame(-1) == 0);
    BOOST_TEST(animation.next_frame(1) == 2);
    BOOST_TEST(animation.next_frame(5, 2) == 7);
    BOOST_TEST(animation.next_frame(5, 9) == 6);

    BOOST_TEST(animation.fps() == 24);
    BOOST_CHECK(animation.frame_time() == std::chrono::milliseconds(41));

    BOOST_TEST(!animation.is_last_frame(8));
}

BOOST_AUTO_TEST_CASE( test_animation_not_looping )
{
    int size = 30, count_x = 7, count_y = 4;
    Image image(Size(count_x*size, count_y*size));
    auto frameset = std::make_shared<SpriteSheet>(image, count_x, count_y);

    Animation animation(frameset, frameset->index_at_grid(0, 2), 8, 24, Animation::NoLoop);
    BOOST_CHECK(animation.frame(0).rect() == frameset->image_at_grid(0, 2).rect());
    BOOST_CHECK(animation.frame(1).rect() == frameset->image_at_grid(1, 2).rect());
    BOOST_CHECK(animation.frame(7).rect() == frameset->image_at_grid(0, 3).rect());

    BOOST_TEST(animation.next_frame(0, 0) == 0);
    BOOST_TEST(animation.next_frame(-1) == 0);
    BOOST_TEST(animation.next_frame(1) == 2);
    BOOST_TEST(animation.next_frame(5, 2) == 7);
    BOOST_TEST(animation.next_frame(5, 9) == 7);

    BOOST_TEST(animation.fps() == 24);
    BOOST_CHECK(animation.frame_time() == std::chrono::milliseconds(41));

    BOOST_TEST(animation.is_last_frame(8));
}

BOOST_AUTO_TEST_CASE( test_animation_random_still )
{
    int size = 30, count_x = 7, count_y = 4;
    Image image(Size(count_x*size, count_y*size));
    auto frameset = std::make_shared<SpriteSheet>(image, count_x, count_y);

    Animation animation(frameset, frameset->index_at_grid(0, 2), 8, 0, Animation::Randomized);
    BOOST_CHECK(animation.frame(0).rect() == frameset->image_at_grid(0, 2).rect());
    BOOST_CHECK(animation.frame(1).rect() == frameset->image_at_grid(1, 2).rect());
    BOOST_CHECK(animation.frame(7).rect() == frameset->image_at_grid(0, 3).rect());

    BOOST_TEST(animation.next_frame(0, 0) >= 0);
    BOOST_TEST(animation.next_frame(-1) >= 0);
    BOOST_TEST(animation.next_frame(1) == 1);
    BOOST_TEST(animation.next_frame(5, 2) == 5);
    BOOST_TEST(animation.next_frame(5, 9) == 5);

    BOOST_TEST(animation.fps() == 0);
    BOOST_CHECK(animation.frame_time() == Duration::zero());
}

BOOST_AUTO_TEST_CASE( test_animation_reverse )
{
    int size = 30, count_x = 7, count_y = 4;
    Image image(Size(count_x*size, count_y*size));
    auto frameset = std::make_shared<SpriteSheet>(image, count_x, count_y);

    Animation animation(frameset, frameset->index_at_grid(0, 2), 8, 24);

    BOOST_TEST(animation.next_frame(0, -1) == 7);
    BOOST_TEST(animation.next_frame(-1, -1) == 6);
    BOOST_TEST(animation.next_frame(1, -1) == 0);
    BOOST_TEST(animation.next_frame(7, -2) == 5);
    BOOST_TEST(animation.next_frame(6, -9) == 5);
    BOOST_TEST(animation.next_frame(8, -1) == 7);

    BOOST_TEST(animation.fps() == 24);
    BOOST_CHECK(animation.frame_time() == std::chrono::milliseconds(41));

    BOOST_TEST(!animation.is_last_frame(8));
}
