/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#define BOOST_TEST_MODULE melanoengine

#include "test.hpp"

#include "scripting/interpreter.hpp"
#include "melanolib/color/color.hpp"

using namespace boost::python;

template<class T, class ObjT>
T to_cpp(const ObjT& value)
{
    return extract<T>(value);
}

template<class Component=float, class ObjT>
melanolib::math::Vec3<Component> to_vec3(const ObjT& value)
{
    return {to_cpp<Component>(value[0]),
            to_cpp<Component>(value[1]),
            to_cpp<Component>(value[2])};
}

void print_error()
{
    PyObject *ptype, *pvalue, *ptraceback;
    PyErr_Fetch(&ptype, &pvalue, &ptraceback);
    PyErr_NormalizeException(&ptype, &pvalue, &ptraceback);

    handle<> exc_handle(pvalue);
    object exception(exc_handle);

    std::cerr << to_cpp<std::string>(object(handle<>(ptype)).attr("__name__"))
              << ": " << to_cpp<std::string>(str(exception)) << '\n';
}


object call_kwargs(const object& callable, std::map<std::string, object> values)
{
    dict kwargs;
    for ( const auto p : values )
        kwargs[p.first] = p.second;
    return callable(*make_tuple(), **kwargs);
}


BOOST_AUTO_TEST_CASE(test_color) try
{
    using namespace melanolib::color;

    scripting::Interpreter interpreter;

    Color cpp_color(0, 128, 255);
    object py_color(ptr(&cpp_color));

    Color cpp_color2(1, 128, 255);
    object py_color2(ptr(&cpp_color2));

    BOOST_TEST(to_cpp<int>(py_color.attr("red")) == cpp_color.red());
    BOOST_TEST(to_cpp<int>(py_color.attr("green")) == cpp_color.green());
    BOOST_TEST(to_cpp<int>(py_color.attr("blue")) == cpp_color.blue());
    BOOST_TEST(to_cpp<int>(py_color.attr("alpha")) == cpp_color.alpha());
    BOOST_TEST(to_cpp<float>(py_color.attr("alpha_float")) == cpp_color.alpha_float());
    BOOST_TEST(to_cpp<int>(py_color.attr("valid")) == cpp_color.valid());

    BOOST_TEST((py_color != py_color2));
    BOOST_TEST((py_color == object(ptr(&cpp_color))));

    BOOST_TEST(to_cpp<std::string>(py_color.attr("__str__")()) == cpp_color.format());
    BOOST_TEST(to_cpp<float>(py_color.attr("distance")(py_color2))
        == cpp_color.distance(cpp_color2));

    BOOST_TEST(to_cpp<Color>(py_color.attr("blend")(py_color2, 0.2))
        == cpp_color.blend(cpp_color2, 0.2));
    BOOST_TEST(to_cpp<Color>(py_color.attr("blend")(py_color2))
        == cpp_color.blend(cpp_color2));
    BOOST_TEST(to_cpp<std::string>(py_color.attr("format")("{g:02x}"))
        == cpp_color.format("{g:02x}"));

    BOOST_TEST(to_vec3<uint8_t>(py_color.attr("rgb")) == cpp_color.to<repr::RGB>().vec());
    BOOST_TEST(to_vec3(py_color.attr("rgb_float")) == cpp_color.to<repr::RGBf>().vec());
    BOOST_TEST(to_vec3(py_color.attr("hsv")) == cpp_color.to<repr::HSVf>().vec());
    BOOST_TEST(to_vec3(py_color.attr("lab")) == cpp_color.to<repr::Lab>().vec());
    BOOST_TEST(to_vec3(py_color.attr("xyz")) == cpp_color.to<repr::XYZ>().vec());
    BOOST_TEST(to_cpp<uint32_t>(py_color.attr("int24"))
        == cpp_color.to<repr::RGB_int24>().vec());
    BOOST_TEST(to_cpp<uint16_t>(py_color.attr("int12"))
        == cpp_color.to<repr::RGB_int12>().vec());
    BOOST_TEST(to_cpp<uint8_t>(py_color.attr("int3"))
        == cpp_color.to<repr::RGB_int3>().vec());
}
catch ( const error_already_set& ) { print_error(); throw; }


BOOST_AUTO_TEST_CASE(test_color_init) try
{
    using namespace melanolib::color;
    auto pyclass = import("melanoengine.graphics").attr("Color");

    BOOST_TEST(to_cpp<Color>(pyclass()) == Color());

    BOOST_TEST(to_cpp<Color>(pyclass(1, 2, 3)) == Color(1, 2, 3));
    BOOST_TEST(to_cpp<Color>(pyclass(1, 2, 3, 4)) == Color(1, 2, 3, 4));
    BOOST_TEST(to_cpp<Color>(pyclass(0.1, 0.2, 0.3))
        == Color(repr::RGBf(0.1, 0.2, 0.3)));
    BOOST_TEST(to_cpp<Color>(pyclass(0.1, 0.2, 0.3, 0.4))
        == Color(repr::RGBf(0.1, 0.2, 0.3), 0.4));

    BOOST_TEST(to_cpp<Color>(
        call_kwargs(pyclass, {{"r", object(1)}, {"g", object(2)}, {"b", object(3)}}))
        == Color(1, 2, 3));
    BOOST_TEST(to_cpp<Color>(call_kwargs(pyclass,
        {{"r", object(1)}, {"g", object(2)}, {"b", object(3)}, {"a", object(4)}}))
        == Color(1, 2, 3, 4));
    BOOST_TEST(to_cpp<Color>(call_kwargs(pyclass,
        {{"r", object(0.1)}, {"g", object(0.2)}, {"b", object(0.3)}}))
        == Color(repr::RGBf(0.1, 0.2, 0.3)));
    BOOST_TEST(to_cpp<Color>(call_kwargs(pyclass,
        {{"r", object(0.1)}, {"g", object(0.2)}, {"b", object(0.3)}, {"a", object(0.4)}}))
        == Color(repr::RGBf(0.1, 0.2, 0.3), 0.4));
    BOOST_TEST(to_cpp<Color>(call_kwargs(pyclass,
        {{"h", object(0.1)}, {"s", object(0.2)}, {"v", object(0.3)}}))
        == Color(repr::HSVf(0.1, 0.2, 0.3)));
    BOOST_TEST(to_cpp<Color>(call_kwargs(pyclass,
        {{"h", object(0.1)}, {"s", object(0.2)}, {"v", object(0.3)}, {"a", object(0.4)}}))
        == Color(repr::HSVf(0.1, 0.2, 0.3), 0.4));
}
catch ( const error_already_set& ) { print_error(); throw; }
