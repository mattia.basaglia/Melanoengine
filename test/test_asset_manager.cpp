/**
 * \file
 *
 * \author Mattia Basaglia
 *
 * \copyright Copyright (C) 2016-2017 Mattia Basaglia
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include "test.hpp"

#include "backend/dummy/graphics.hpp"
#include "backend/common/backend.hpp"
#include "game/asset_manager.hpp"
#include "engine/data.hpp"

using game::AssetManager;

bool init_unit_test()
{
    backend::dummy::DummyGraphics::register_dummy();
    backend::Backend::instance().initialize("dummy");
    engine::Data::instance().add_path(TEST_DATA_PATH);
    return true;
}

int main(int argc, char* argv[])
{
    return boost::unit_test::unit_test_main( &init_unit_test, argc, argv );
}

BOOST_AUTO_TEST_CASE( test_images )
{
    AssetManager manager;
    BOOST_TEST(manager.images() == 0);
    BOOST_TEST(manager.images_all() == 0);
    manager.set_images_max(5);
    BOOST_TEST(manager.images_max() == 5);
    BOOST_TEST(manager.image("foo1"));
    BOOST_TEST(manager.image("foo2"));
    BOOST_TEST(manager.image("foo3"));
    BOOST_TEST(manager.images() == 3);
    BOOST_TEST(manager.images_all() == 3);
    auto foo3 = manager.image("foo3");
    BOOST_TEST(foo3);
    BOOST_TEST(manager.image("foo1"));
    BOOST_TEST(manager.images() == 3);
    BOOST_TEST(manager.images_all() == 3);
    BOOST_TEST(manager.image("foo4"));
    BOOST_TEST(manager.image("foo5"));
    BOOST_TEST(manager.image("foo6"));
    BOOST_TEST(manager.image("foo7"));
    BOOST_TEST(manager.images() == 5);
    BOOST_TEST(manager.images_all() == 6);
    manager.set_images_max(3);
    BOOST_TEST(manager.images_max() == 3);
    BOOST_TEST(manager.images() == 3);
    BOOST_TEST(manager.images_all() == 4);
    foo3.reset();
    BOOST_TEST(manager.images() == 3);
    BOOST_TEST(manager.images_all() == 3);
}
