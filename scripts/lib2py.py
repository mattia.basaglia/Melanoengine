#!/usr/bin/env python3
import re
import os
import imp
import sys
import inspect
import itertools
import types
import argparse


def load_module(file_path):
    """
    \brief Loads a python module from its path
    \param file_path path to a module file, which might be a dynamic library
    """
    path, filename = os.path.split(file_path)
    basename = os.path.splitext(filename)[0]
    return imp.load_module(basename, *imp.find_module(basename, [path]))


class PyFormatter:
    """
    \brief Output formatter class
    """
    def __init__(self, output, annotate_types=False, indent_step=" " * 4, indentation=""):
        """
        \param output           Output file object
        \param annotate_types   Whether to add Python3 type annotations
        \param indent_step      String to use to indent by one level
        \param indentation      Current line prefix
        """
        self.output = output
        self.annotate_types = annotate_types
        self.indent_step = indent_step
        self.indentation = indentation

    def child(self):
        """
        Returns a PyFormatter with one extra indentation level
        """
        return PyFormatter(
            self.output,
            self.annotate_types,
            self.indent_step,
            self.indentation + self.indent_step
        )

    def write(self, line):
        """
        \brief Prints the given line with the current indentation
        (adding a newline character)
        """
        self.output.write("%s%s\n" % (self.indentation, line))

    def skip_line(self):
        """
        \brief Prints an empty line
        """
        self.output.write("\n")

    def annotate_type(self, prefix, typestring):
        """
        \brief Depending on self.annotate_types, it will output the correct
        type annotation
        \param prefix           Prefix to add to the type when annotations are enabled
        \param typestring       Type as a string
        """
        if not self.annotate_types:
            return ""
        if typestring not in dir(__builtins__):
            typestring = repr(typestring)
        return prefix + typestring



    def write_docstring(self, object):
        """
        \brief Writes a docstring
        \param object to extract the docstring from or string to display
        """
        if type(object) is str:
            docstring = inspect.cleandoc(object)
        elif object.__doc__:
            docstring = inspect.getdoc(object)
        else:
            docstring = ""

        # TODO: Escape '
        if docstring:
            self.write("'''")
            for line in docstring.splitlines():
                self.write(line)
            self.write("'''")


def is_builtin_type(object):
    """
    \brief Whether the given object is a built-in type (eg: int, float)
    """
    return object in vars(__builtins__).values()


def is_builtin_literal(string):
    """
    \brief Whether the given string is a valid literal for a built-in type
    \see is_builtin_type()
    """
    try:
        return is_builtin_type(type(eval(string)))
    except:
        return False


class WrappedRepr:
    """
    Wrapped value representation
    """
    def __init__(self, value):
        self.value = value

    def __repr__(self):
        return self.value


def value_repr(value, module, enum=None, enum_byname=False):
    """
    \brief Creates a proper representation of the given value, using built-in
           literals and stubs for custom classes
    \param value        Value to get the representation of
    \param module       Module wrapper to extract type names
    \param enum         Type to treat as enumeration (for Boost python enum wrappers)
    \param enum_byname  Whether to use names instead of integers for enum values
    """
    sequence_types = {list, tuple, set}
    map_types = {dict}
    vtype = type(value)

    if vtype in sequence_types:
        value = vtype(value_repr(item, module, enum, True) for item in value)
    elif vtype in map_types:
        value = vtype(
            (value_repr(k, module, enum, True), value_repr(v, module, enum, True))
            for k, v in value.items()
        )
    elif inspect.isclass(value):
        return value.__name__
    elif not is_builtin_type(vtype):
        type_name = module.type_name(vtype)
        if isinstance(value, int):
            if vtype is not enum:
                return WrappedRepr("%s(%s)" % (type_name, int(value)))
            elif enum_byname:
                return WrappedRepr(str(value))
            value = int(value)
        else:
            return WrappedRepr("%s()" % (type_name))
    return WrappedRepr(repr(value))


class WrappedSymbol:
    """
    Base class for wrappers of symbols loaded from a library
    """
    # Class-level ordering of the symbol (eg: to sort classes before functions)
    type_order = 0

    def __init__(self, name, module):
        """
        \param name     Name of the symbol
        \param module   Wrapper of the module this symbol belongs to
        """
        self.name = name
        self.module = module
        self.item_order = 0         # Set by the module wrapper to resolve dependencies
        self.extra_sort_value = 0   # Customizable on an instance-basis to override or add upon dependency sort

    def resolve_types(self):
        """
        \brief Used when types are known to the module wrapper, so type names
        can be reursively resolved
        """
        raise NotImplementedError()

    def write(self, formatter):
        """
        \brief Writes the symbol out using \p formatter
        """
        raise NotImplementedError()

    def __lt__(self, other):
        """
        \brief Comparator used to sort lists of wraped symbols
        \see compare_key
        """
        return self.compare_key() < other.compare_key()

    def compare_key(self):
        """
        Tuple used in comparisons
        """
        return (self.type_order, self.item_order + self.extra_sort_value, self.name)

    def all_dependencies(self):
        """
        \brief Returns a raw list of dependencies, items can be string or types

        Child classes should override this method, not dependencies()
        """
        return []

    def dependencies(self):
        """
        \brief Clean dependency set
        \returns A set of strings representing the type names this object depends on
        """
        return set(
            self.dependency_name(dep)
            for dep in self.all_dependencies()
        ) - set([None]) - self.provided_dependencies()

    def dependency_name(self, dep):
        """
        \brief Returns a clean dependency name from \p dep
        \param dep Dependency name or type
        \returns A string with the intra-module dependency name, or None if
                 \p dep does not appear to be defined in the parrent module
        """
        if type(dep) is str:
            if dep.startswith(self.module.name+"."):
                return dep[len(self.module.name)+1:]
            return dep
        if is_builtin_type(dep):
            return None
        if dep.__module__ != self.module.name:
            return None
        return dep.__name__

    def provided_dependencies(self):
        """
        Set of dependency names provided by the symbols
        """
        return set()

    def __repr__(self):
        return "wrapped %s %s" % (self.__class__.__name__, self.name)


class Object(WrappedSymbol):
    """
    \bref Wraps an object / variable
    """
    type_order = 10

    def __init__(self, name, value, module):
        WrappedSymbol.__init__(self, name, module)
        self.type = type(value)
        self.value = value
        self.extra_sort_value = int(value) if isinstance(value, int) else 2 ** 64

    def resolve_types(self):
        self.value = value_repr(self.value, self.module)

    def write(self, formatter):
        formatter.write("%s = %s" % (self.name, self.value))

    def all_dependencies(self):
        return [self.type]


class Attribute(Object):
    """
    \brief Similar to  Object, but provides itself as a dependency and with a different type order
    """
    type_order = 2

    def __init__(self, owner, name, value, module):
        Object.__init__(self, name, value, module)
        self.owner = owner

    def resolve_types(self):
        self.value = value_repr(self.value, self.module, self.owner)

    def provided_dependencies(self):
        name = self.name
        if self.owner != self.module.module:
            name = self.owner.name + '.' + name
        return set([name])


class Class(WrappedSymbol):
    """
    \brief Class wrapper
    """
    type_order = 5

    def __init__(self, cls, module):
        WrappedSymbol.__init__(self, cls.__name__, module)
        self.members = []
        self.bases = []
        self.cls = cls

        for name, value in vars(cls).items():
            if name in {"__module__", "__doc__", "__weakref__", "__dict__"}:
                continue
            self.members += self._resolve_member(name, value)

    def _resolve_member(self, name, value):
        attribute = lambda *a : Attribute(self.cls, *a)

        if isinstance(value, staticmethod):
            return wrap_object(name, getattr(self.cls, name), self.module, attribute, "", ["staticmethod"])
        elif isinstance(value, classmethod):
            return wrap_object(name, getattr(self.cls, name), self.module, attribute, "cls", ["classmethod"])
        elif isinstance(value, property):
            get_value = getattr(self.cls, name)
            if inspect.isfunction(get_value) or get_value is value:
                return wrap_object(name, get_value, self.module, attribute, "self")
            else:
                return self._resolve_member(name, get_value)
        else:
            return wrap_object(name, value, self.module, attribute, "self")

    def resolve_types(self):
        for base in self.cls.__bases__:
            self.bases.append(self.module.type_name(base))

        for value in self.members:
            value.resolve_types()

    def write(self, formatter):

        formatter.write("class %s(%s):" % (
            self.cls.__name__, ", ".join(self.bases)
        ))
        formatter.child().write_docstring(self.cls)
        if self.cls.__doc__:
            formatter.skip_line()

        if not self.members:
            formatter.child().write("pass")
        else:
            for value in sorted(self.members):
                value.write(formatter.child())
        formatter.skip_line()

    def all_dependencies(self):
        return sum((m.all_dependencies() for m in self.members), self.bases)

    def provided_dependencies(self):
        return set(
            [self.name + "." + m.name for m in self.members] +
            [self.name]
        )


class Function(WrappedSymbol):
    """
    \brief Python function wrapper
    """
    type_order = 7

    def __init__(self, name, function, module, decorators=[]):
        # NOTE: not using function.__name__ to include lambdas
        WrappedSymbol.__init__(self, name, module)
        self.docstring = function.__doc__
        self.decorators = decorators
        self.argstr = str(inspect.signature(function))
        self.attributes = [
            Object(name + "." + attr, value, module)
            for attr, value in sorted(vars(function).items())
        ]
        self.body = "pass"

    def resolve_types(self):
        pass

    def write(self, formatter):
        for decorator in self.decorators:
            formatter.write("@" + decorator)
        formatter.write("def %s%s:" % (self.name, self.signature(formatter)))
        formatter.child().write_docstring(self.docstring)
        formatter.child().write(self.body)
        for attribute in self.attributes:
            attribute.write(formatter.child())
        formatter.skip_line()

    def signature(self, formatter):
        return self.argstr


class Parameter(WrappedSymbol):
    """
    \brief Function parameter wrapper
    """
    def __init__(self, name, module, type="", default=""):
        WrappedSymbol.__init__(self, name, module)
        self.type = type
        self.default = default

    def all_dependencies(self):
        if self.default and not is_builtin_literal(self.default):
            return [self.default]
        return []

    def formatted(self, formatter):
        """
        \brief Returns the parameter formatted as a string
        """
        string = self.name
        string += formatter.annotate_type(":", self.type)
        if self.default:
            string += "=" + self.default
        return string



class LibFunction(Function):
    """
    \brief Wrapper for functions whose metadate is extracted from a library
    """
    def __init__(self, name, module, args, docstr, return_type, decorators):
        WrappedSymbol.__init__(self, name, module)
        self.decorators = decorators
        self.docstring = docstr
        if return_type:
            if return_type != "None":
                return_type += "()"
            self.body = "return " + return_type
        else:
            self.body = "pass"
        self.attributes = []
        self.args = args
        self.return_type = return_type

    def all_dependencies(self):
        return sum((arg.all_dependencies() for arg in self.args), [])

    def signature(self, formatter):
        return "(%s)%s" % (
            ", ".join(arg.formatted(formatter) for arg in self.args),
            formatter.annotate_type(" -> ", self.return_type)
        )


class Module:
    """
    \brief Module wrapper
    """
    def __init__(self, module_object):
        self.import_modules = {}
        self.imports = {}
        self.members = []
        self.new_members = []
        # TODO: write out submodules
        self.submodules = set()
        self.available_names = set()
        self.module = module_object
        self.name = module_object.__name__
        self.types = {}

        for name, value in vars(module_object).items():
            if name.startswith("__"):
                continue
            elif inspect.ismodule(value):
                self._import_module(name, value)
                new_members = []
            elif getattr(value, "__module__", self.name) != self.name:
                self._import(name, value)
                new_members = []
            else:
                if inspect.isclass(value):
                    self.types[value] = name
                new_members = wrap_object(name, value, self)
            self.available_names.add(name)
            self.new_members += new_members

        self.resolve()

    def resolve(self):
        """
        \brief Resolves types ad dependencies
        """
        pending_deps = []

        # Resolve types
        while self.new_members:
            current_new = self.new_members
            self.new_members = []
            pending_deps += current_new
            for item in current_new:
                item.resolve_types()

        # Resolve dependencies
        contained_deps = self.provided_dependencies()
        while pending_deps:
            current_batch = sorted(pending_deps)
            pending_deps = []
            added = False

            while current_batch:
                item = current_batch.pop(0)
                item.item_order = len(self.members)
                if item.dependencies() - contained_deps:
                    pending_deps.append(item)
                else:
                    contained_deps |= item.provided_dependencies()
                    self.members.append(item)
                    added = True

            if not added:
                # Circular dependency
                self.members += pending_deps
                pending_deps = []

    def provided_dependencies(self):
        deps = set()
        for member in self.members:
            deps |= member.provided_dependencies()
        return deps

    def type_name(self, type):
        """
        \brief Returns the name associated with the given type

        If \p type is not avalilable, it ensures it gets imported or otherwise defined
        """
        if type in self.types:
            return self.types[type]
        if not is_builtin_type(type):
            if type.__module__ == "Boost.Python":
                self.new_members.append(Attribute(
                    self.module,
                    type.__name__,
                    int if type.__name__ == "enum" else object,
                    self
                ))
                self.types[type] = type.__name__
            else:
                self._import(type.__name__, type)
        return type.__name__

    def _import_module(self, name, value):
        """
        \brief Imports a module
        \param name     Name to import as
        \param value    Module object
        """
        real_name = value.__name__
        if real_name.startswith(self.name + "."):
            real_name = real_name[len(self.name):]
            module_name, real_name = real_name.rsplit(".", 1)
            module_name = "." + module_name
            self.imports.setdefault(module_name, {})
            self.imports[module_name][real_name] = name
            self.submodules.add(value)
        else:
            self.imports.setdefault(real_name, {})
            self.import_modules[real_name] = name

    def _import(self, name, value):
        """
        \brief Imports a symbol
        \param name     Name to import as
        \param value    Symbol to import
        """
        module_name = inspect.getmodule(value).__name__
        real_name = value.__name__

        if module_name.startswith(self.name + "."):
            module_name = module_name[len(self.name):]

        if inspect.isclass(value):
            self.types[value] = name
        self.imports.setdefault(module_name, {})
        self.imports[module_name][real_name] = name

    def write(self, formatter):
        """
        \brief Writes out the modules
        """
        self.resolve()

        formatter.child().write_docstring(self.module)
        for module, items in sorted(self.imports.items()):
            if module in self.import_modules:
                import_str = "import %s" % module
                if self.import_modules[module] != module:
                    import_str += " as %s" % self.import_modules[module]
                formatter.write(import_str)

            if items:
                formatter.write("from %s import %s" % (
                    module,
                    ", ".join(
                        item[0] if item[0] == item[1] else "%s as %s" % item
                        for item in sorted(items.items())
                    )
                ))
        formatter.skip_line()

        exported_all = self.available_names
        if hasattr(self.module, "__all__"):
            exported_all = self.module.__all__
        formatter.write("__all__ = [%s]" % ", ".join(sorted(map(repr, exported_all))))
        formatter.skip_line()
        formatter.skip_line()

        for value in sorted(self.members):
            value.write(formatter)
            formatter.skip_line()


# Regex used to extract parameter information from a boost python signature comment
boost_py_sig_params = re.compile(r"\s*\(([^()]+)\)([a-zA-Z_0-9]+)(?:=([^\], ]+))?\s*")
# Regex used to extract the return type from a boost python signature comment
boost_py_sig_return  = re.compile(r"\s*->\s*(\w*)\s*:$")
def boost_python_overloads(name, function, module, extra_arg="", decorators=[]):
    """
    \brief Parses the signature comment in boost-python-wrapped functions
    \param name         Function name
    \param function     Function object
    \param module       Parent module wrapper
    \param extra_arg    "self", "cls" or similar to force the name of the argument
    \param decorators   List of decorators as strings
    \returns A list of LibFunction with all the available overloads
    """
    def strip_module(symbol):
        """
        \brief Remove the module prefix frpm \p symbol
        \returns The stripped string
        """
        return symbol.replace(module.name + '.', "")

    # Get the docstring
    docs = inspect.getdoc(function)
    if not docs:
        # No docstring => unknown arguments
        params = [Parameter("*UNKNOWN", module)]
        if extra_arg:
            params.insert(0, Parameter(extra_arg, module))
        return [
            LibFunction(name, module, params, "", "", decorators)
        ]

    overloads = []
    try:
        lineit = iter(docs.splitlines())
        while True:
            line = next(lineit)
            if line.startswith("%s(" % name):
                params = [
                    Parameter(pname, module, strip_module(type), strip_module(default))
                    for type, pname, default in boost_py_sig_params.findall(line)
                ]
                if params and extra_arg:
                    params[0].name = extra_arg

                # Get the return value
                return_type = ""
                match = boost_py_sig_return.search(line)
                if match:
                    return_type = match.group(1)
                # Get docstring until an empty line is reached
                docstr = ""
                while True:
                    line = next(lineit)
                    if not line:
                        break
                    docstr += line
                # Skip C++ signature
                next(lineit)
                next(lineit)
                # Add the overload
                overloads.append(LibFunction(name, module, params, docstr, return_type, decorators))
    except StopIteration:
        pass

    return overloads


def wrap_object(name, value, module, on_variable=Object, extra_arg="", decorators=[]):
    """
    \brief Wraps an object into the correct type
    \returns A list of WrappedSymbol corresponding to the declarations needed
             to produce \p value
    \param name         Object name
    \param value        Object value
    \param module       Parent module wrapper
    \param on_variable  Callable to use (passing name, value, module) on generic variable assignment
    \param extra_arg    Optional "self" for boost_python_overloads
    \param decorators   Function decorators
    """
    if isinstance(value, types.BuiltinFunctionType):
        return boost_python_overloads(name, value, module, extra_arg, decorators)
    elif inspect.isfunction(value):
        return [Function(name, value, module, decorators)]
    elif inspect.isclass(value):
        return [Class(value, module)]
    elif isinstance(value, property):
        getters = wrap_object(name, value.fget, module, on_variable, extra_arg, ["property"])

        setters = []
        if value.fset:
            setters = wrap_object(name, value.fset, module, on_variable, extra_arg, [name + ".setter"])

        overloads = getters + setters
        for overload in overloads:
            if isinstance(overload, LibFunction):
                overload.args = [Parameter(extra_arg, module)] if extra_arg else []
            else:
                overload.argstr = "(%s)" % extra_arg
            overload.docstring = value.__doc__

        for setter in setters:
            if isinstance(setter, LibFunction):
                setter.args.append(Parameter("value", module))
            else:
                setter.argstr.replace(")", ", value)")
        return overloads
    else:
        return [on_variable(name, value, module)]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Produces a python script with declarations from a python module"
    )
    parser.add_argument(
        "module",
        help="Path to the module file to load",
    )
    parser.add_argument(
        "-o", "--output",
        help="Output file path",
        default="",
    )
    parser.add_argument(
        "--indent-width",
        type=int,
        default=4,
        help="Indentation width",
    )
    parser.add_argument(
        "--annotate-types", "-3",
        action="store_true",
        help="Enables Python 3 type annotations",
    )
    parser.add_argument(
        "--no-annotate-types", "-2",
        action="store_false",
        dest="annotate_types",
        help="Disables Python 3 type annotations",
    )
    parser.add_argument(
        "--submodule", "-s",
        help="Dotted path to a nested module",
        default="",
    )

    options = parser.parse_args()

    try:
        module = load_module(options.module)
        for submodule in filter(bool, options.submodule.split(".")):
            module = getattr(module, submodule)
        if not isinstance(module, types.ModuleType):
            raise TypeError()
    except (AttributeError, TypeError):
        sys.stderr.write("Could not find module %s in %s\n" % (options.submodule, options.module))
        sys.exit(1)

    Module(module).write(PyFormatter(
        sys.stdout if not options.output or options.output == "-" else open(options.output),
        options.annotate_types,
        " " * options.indent_width
    ))
